﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeTools
{
    public class ImageUtils
    {

        public static Bitmap Base64StringToBitmap(string base64String)
        {
            base64String = base64String.Substring(base64String.IndexOf(',')+1);
            Bitmap bmpReturn = null;
            //Convert Base64 string to byte[]
            byte[] byteBuffer = Convert.FromBase64String(base64String);
            MemoryStream memoryStream = new MemoryStream(byteBuffer);

            memoryStream.Position = 0;

            bmpReturn = (Bitmap)Bitmap.FromStream(memoryStream);

            memoryStream.Close();
            memoryStream = null;
            byteBuffer = null;

            return bmpReturn;
        }


        public static void SaveImage(string FileName, Bitmap myBitmap)
        {


            Bitmap newBitmap = new Bitmap(myBitmap);
            myBitmap.Dispose();
            myBitmap = null;
            try
            {
                if (System.IO.File.Exists(FileName))
                    System.IO.File.Delete(FileName);
                newBitmap.Save(FileName, ImageFormat.Jpeg);
            }
            catch (Exception ex)
            {
                //do nothing 
            }



            //int width = Convert.ToInt32(myBitmap.Width);
            //int height = Convert.ToInt32(myBitmap.Height);
            //Bitmap bmp = new Bitmap(width, height);
            //myBitmap.(bmp, new Rectangle(0, 0, width, height);



            //if (System.IO.File.Exists(FileName))
            //    System.IO.File.Delete(FileName);
            //myBitmap.Save(FileName, ImageFormat.Jpeg);


            //ImageCodecInfo myImageCodecInfo;
            //System.Drawing.Imaging.Encoder myEncoder;
            //EncoderParameter myEncoderParameter;
            //EncoderParameters myEncoderParameters;

            //// Get an ImageCodecInfo object that represents the JPEG codec.
            //myImageCodecInfo = ExchangeTools.ImageUtils.GetEncoderInfo("image/jpeg");

            //// Create an Encoder object based on the GUID

            //// for the Quality parameter category.
            //myEncoder = System.Drawing.Imaging.Encoder.Quality;

            //// Create an EncoderParameters object.

            //// An EncoderParameters object has an array of EncoderParameter

            //// objects. In this case, there is only one

            //// EncoderParameter object in the array.
            //myEncoderParameters = new EncoderParameters(1);

            //// Save the bitmap as a JPEG file with quality level 25.
            //myEncoderParameter = new EncoderParameter(myEncoder, 90L);
            //myEncoderParameters.Param[0] = myEncoderParameter;
            //if (System.IO.File.Exists(FileName))
            //    System.IO.File.Delete(FileName);
            //myBitmap.Save(FileName, myImageCodecInfo, myEncoderParameters);

            ////// Save the bitmap as a JPEG file with quality level 50.
            ////myEncoderParameter = new EncoderParameter(myEncoder, 50L);
            ////myEncoderParameters.Param[0] = myEncoderParameter;
            ////myBitmap.Save(saveFileDialog.FileName, myImageCodecInfo, myEncoderParameters);

            ////// Save the bitmap as a JPEG file with quality level 75.
            ////myEncoderParameter = new EncoderParameter(myEncoder, 75L);
            ////myEncoderParameters.Param[0] = myEncoderParameter;
            ////myBitmap.Save(saveFileDialog.FileName, myImageCodecInfo, myEncoderParameters);

        }

        public static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

    }
}
