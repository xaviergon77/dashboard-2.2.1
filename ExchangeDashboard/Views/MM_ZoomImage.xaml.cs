﻿using ExchangeDashboard.ViewModels;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExchangeDashboard.Views
{
    /// <summary>
    /// Interaction logic for MM_ZoomImage.xaml
    /// </summary>
    public partial class MM_ZoomImage : Window
    {
        public Bitmap image;
        public MM_ZoomImage()
        {
            InitializeComponent();

        }

        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() == true)
            {
                string FileName = saveFileDialog.FileName;
                if (
                    !FileName.EndsWith(".jpg", StringComparison.InvariantCultureIgnoreCase)
                    && !FileName.EndsWith(".jpeg", StringComparison.InvariantCultureIgnoreCase)
                    )
                {
                    FileName += ".jpg";
                }
                ExchangeTools.ImageUtils.SaveImage(FileName, ((MM_ZoomImage_ViewModel)this.DataContext).Image);
            }
        }
    }
}
