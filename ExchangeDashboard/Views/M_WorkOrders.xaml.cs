﻿using DAI.CORE.UIM.UIM_WorkOrder;
using ExchangeDashboard.Lib;
using ExchangeDashboard.ViewModels;
using ExchangeDB;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExchangeDashboard
{

    /// <summary>
    /// Interaction logic for M_Users.xaml
    /// </summary>
    public partial class M_WorkOrders : Page
    {
        List<string> YOURDATASOURCE = new List<string>() { "Block", "Review", "Move", "Submit" };
        List<Window> OwnedWindows = new List<Window>() { };

        public M_WorkOrders()
        {
            InitializeComponent();
            Lib.Util.CheckIfAuthenticated();
            MM_PropertyAdd modalWindow = new MM_PropertyAdd();
            modalWindow.Owner = App.Current.MainWindow;
            OwnedWindows.Add(modalWindow);
            Loaded += Page_Loaded;
            App.Current.MainWindow.LocationChanged += new EventHandler(Window_LocationChanged);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (gridUsers.Items.Count == 0)
            {
                //((M_WorkOrders_ViewModel)this.DataContext).LoadWorkOrders();
                //((M_WorkOrders_ViewModel)this.DataContext).SelectedWorkOrder = null;
            }
        }

        private void Users_Rows_Selected(object sender, RoutedEventArgs e)
        {
            if (gridUsers.SelectedItems.Count == 1)
            {
                ((M_WorkOrders_ViewModel)this.DataContext).SelectedWorkOrder = (UIMWorkOrder_detail_response_simple)gridUsers.SelectedItem;

            }
        }

        private void StackPanel_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

        }


        private void PageKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                MessageBox.Show("user is pressed Ctrl+S");
            }
        }

        private void btnAddProp_Click(object sender, RoutedEventArgs e)
        {
            //if (!OwnedWindows[0].IsLoaded)
            //{
            //    OwnedWindows[0] = new MM_PropertyAdd();
            //    OwnedWindows[0].Owner = App.Current.MainWindow;
            //}
            //if (OwnedWindows[0].ShowDialog() == true)
            //{
            //    var something = OwnedWindows[0].DialogResult;
            //    tbl_property_val x = ((MM_UserProperty_ViewModel)OwnedWindows[0].DataContext).PropertyVal;
            //}

        }

        private void Window_LocationChanged(object sender, EventArgs e)
        {
            foreach (Window win in this.OwnedWindows)
            {
                win.Top = App.Current.MainWindow.Top + 100;
                win.Left = App.Current.MainWindow.Left + 100;
            }
        }

        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            //M_EndUsers page2 = new M_EndUsers();
            //page2.paramId = ((M_WorkOrders_ViewModel)this.DataContext).SelectedWorkOrder.caT_usr_gen_Id;
            //page2.paramIsApplications = true;
            //this.NavigationService.Navigate(page2);
            Lib.Navigator.NavigateWithState("Views/M_EndUsers.xaml", ((M_WorkOrders_ViewModel)this.DataContext).SelectedWorkOrder);
            //Navigator.Navigate("Views/M_EndUsers.xaml?Id=" + ((M_WorkOrders_ViewModel)OwnedWindows[0].DataContext).SelectedWorkOrder.CAT_usr_gen_Id_Source);
        }
    }
}
