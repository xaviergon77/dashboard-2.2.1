﻿using ExchangeDashboard.Lib;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExchangeDashboard
{
    /// <summary>
    /// Interaction logic for A_TwoFactorRegister.xaml
    /// </summary>
    public partial class A_TwoFactorRegister_2 : Page
    {

        public A_TwoFactorRegister_2()
        {
            InitializeComponent();
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Render, new Action(() =>
            //{
            //    var navWindow = Window.GetWindow(this) as NavigationWindow;
            //    if (navWindow != null) navWindow.ShowsNavigationUI = false;
            //}));
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Views/A_Login.xaml");
        }

        private void btnFinal_Click(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Views/B_Menu.xaml");

        }
    }
}
