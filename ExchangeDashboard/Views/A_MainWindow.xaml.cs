﻿using ExchangeDashboard.Lib;
using ExchangeDashboard.ViewModels;
using Google_Authenticator_netcore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExchangeDashboard
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class A_MainWindow : Window
    {
        //NavigationService NavService;
        internal StatusBar_ViewModel ViewModel = new StatusBar_ViewModel();

        public A_MainWindow()
        {
            InitializeComponent();
            ((A_MainWindow_ViewModel)this.DataContext).Title = "Exchange Dashboard";
            sbar.DataContext = ViewModel;

            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Render, new Action(() =>
            //{
            //    var navWindow = Window.GetWindow(this) as NavigationWindow;
            //    if (navWindow != null) navWindow.ShowsNavigationUI = false;
            //}));
            //MainFrame.Source = new Uri("A_Login.xaml", UriKind.RelativeOrAbsolute);
            //this.Loaded += A_MainWindowLoaded;
            //test();
        }


        private void A_MainWindowLoaded(object sender, RoutedEventArgs e)
        {
            //this.NavService = NavigationService.GetNavigationService(this);
            //Navigator.Navigate("A_Login.xaml");
        }

        public void test()
        {
            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            var setupInfo = tfa.GenerateSetupCode("Exchange", "This is a very interesting app", "esquipulas", 300, 300); //the width and height of the Qr Code in pixels

            string qrCodeImageUrl = setupInfo.QrCodeSetupImageUrl;
            string manualEntrySetupCode = setupInfo.ManualEntryKey; 

            string user_input = Console.ReadLine();
            TwoFactorAuthenticator tfa1 = new TwoFactorAuthenticator();
            bool isCorrectPIN = tfa.ValidateTwoFactorPIN("mysecretkey", "799634", TimeSpan.FromSeconds(10));
            if (isCorrectPIN == true)
            {
                Console.WriteLine("I'm in");
            }
            else
            {
                Console.WriteLine("Failed");
            }
        }

        private void GoWorkOrders(object sender, RoutedEventArgs e)
        {
            MainFrame.Source = new Uri("M_WorkOrders.xaml", UriKind.RelativeOrAbsolute);
        }
        private void GoNewApplications(object sender, RoutedEventArgs e)
        {
            MainFrame.Source = new Uri("M_EndUsers.xaml", UriKind.RelativeOrAbsolute);
        }
        private void GoAdministrativeUsers(object sender, RoutedEventArgs e)
        {
            MainFrame.Source = new Uri("M_AdminUsers.xaml", UriKind.RelativeOrAbsolute);
        }

        private void ClearCache_Click(object sender, RoutedEventArgs e)
        {
            ObjectCache cache = MemoryCache.Default;

            List<string> cacheKeys = cache.Select(kvp => kvp.Key).ToList();
            foreach (string cacheKey in cacheKeys)
            {
                cache.Remove(cacheKey);
            }
            MessageBox.Show("Cache cleared");
        }

        private void Click_Log_Off(object sender, RoutedEventArgs e)
        {
            Lib.Util.LogOff();
        }
    }
}
