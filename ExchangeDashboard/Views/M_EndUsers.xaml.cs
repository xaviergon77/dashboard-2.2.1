﻿using DAI.CORE.UIM.UIM_WorkOrder;
using ExchangeDashboard.Lib;
using ExchangeDashboard.ViewModels;
using ExchangeDashboard.Views;
using ExchangeDB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExchangeDashboard
{

    /// <summary>
    /// Interaction logic for M_Users.xaml
    /// </summary>
    public partial class M_EndUsers : Page
    {
        List<string> YOURDATASOURCE = new List<string>() { "Block", "Review", "Move", "Submit" };
        List<Window> OwnedWindows = new List<Window>() { };
        public ulong paramId = 0;
        public bool paramIsApplications = false;
        public static event EventHandler StatusMessage;

        //public M_EndUsers(UIMWorkOrder_detail_response obj)
        public M_EndUsers()
        {
            InitializeComponent();
            Lib.Util.CheckIfAuthenticated();
            //System.Windows.Application.Current.MainWindow.DataContext = new M_EndUsers_ViewModel(obj as UIMWorkOrder_detail_response);
            //System.Windows.Application.Current.MainWindow.Content = new AddStaticIPAddress();
            //Object obj = Lib.Navigator.GetLastNavigationData(this.NavigationService);

            MM_PropertyAdd modalWindow = new MM_PropertyAdd();
            modalWindow.Owner = App.Current.MainWindow;
            OwnedWindows.Add(modalWindow);
            Loaded += Page_Loaded;
            App.Current.MainWindow.LocationChanged += new EventHandler(Window_LocationChanged);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (gridUsers.Items.Count == 0)
            {
                ((M_EndUsers_ViewModel)this.DataContext).LoadUsers(Lib.Navigator.GetLastNavigationData(this.NavigationService) as UIMWorkOrder_detail_response_simple);
                //gridUsers.SelectedIndex = 0;
                //((M_EndUsers_ViewModel)this.DataContext).SelectedUser = null;
                //for (int i = 0; i < gridUsers.Items.Count; i++)
                //{
                //    if (((UIMWorkOrder_detail_response)gridUsers.Items[i]).Id == paramId)
                //    {
                //        gridUsers.SelectedItem = gridUsers.Items[i];
                //    }
                //}
            }
        }

        private void StackPanel_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void PageKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                MessageBox.Show("user is pressed Ctrl+S");
            }
        }

        private void btnAddProp_Click(object sender, RoutedEventArgs e)
        {
            //if (!OwnedWindows[0].IsLoaded)
            //{
            //    OwnedWindows[0] = new MM_PropertyAdd();
            //    OwnedWindows[0].Owner = App.Current.MainWindow;
            //}
            //if (OwnedWindows[0].ShowDialog() == true)
            //{
            //    var something = OwnedWindows[0].DialogResult;
            //    tbl_property_val x = ((MM_UserProperty_ViewModel)OwnedWindows[0].DataContext).PropertyVal;
            //}
        }

        private void Window_LocationChanged(object sender, EventArgs e)
        {
            foreach (Window win in this.OwnedWindows)
            {
                win.Top = App.Current.MainWindow.Top + 100;
                win.Left = App.Current.MainWindow.Left + 100;
            }
        }

        private void chkApprove_Clicked(object sender, RoutedEventArgs e)
        {
            if (chkApprove.IsChecked == true)
            {
                btnAccount0.Visibility = Visibility.Visible;
            }
            else
            {
                btnAccount0.Visibility = Visibility.Collapsed;
            }
        }

        private void PhoneGotFocus(object sender, RoutedEventArgs e)
        {
            if (((Control)sender).Name.Contains("phone", StringComparison.InvariantCultureIgnoreCase))
            {
                StatusMessage?.Invoke("Include country code, example: +506 555 2939 1021", null);
            }
        }

        private void btnDocument_Click(object sender, RoutedEventArgs e)
        {
            string path = ((Button)sender).Tag.ToString();
            if (
                path.EndsWith(".jpg", StringComparison.InvariantCultureIgnoreCase)
                || path.EndsWith(".jpeg", StringComparison.InvariantCultureIgnoreCase)
                || path.EndsWith(".png", StringComparison.InvariantCultureIgnoreCase)
                || path.EndsWith(".bmp", StringComparison.InvariantCultureIgnoreCase)
                )
            {
                OwnedWindows[0] = new MM_ZoomImage();
                OwnedWindows[0].Owner = App.Current.MainWindow;
                ((MM_ZoomImage_ViewModel)OwnedWindows[0].DataContext).Path = path;
            }
            //else if (
            //    path.EndsWith(".pdf", StringComparison.InvariantCultureIgnoreCase)
            //    )
            //{
            //    OwnedWindows[0] = new MM_ZoomPdf();
            //    OwnedWindows[0].Owner = App.Current.MainWindow;
            //    ((MM_ZoomPdf_ViewModel)OwnedWindows[0].DataContext).Path = path;
            //}

            if (OwnedWindows != null && OwnedWindows.Count > 0 && OwnedWindows[0].ShowDialog() == true)
            {
                var something = OwnedWindows[0].DialogResult;
            }
        }

        private void DoFilter(object sender, RoutedEventArgs e)
        {
            if (((M_EndUsers_ViewModel)this.DataContext).SelectedFilter.Contains("approved", StringComparison.InvariantCultureIgnoreCase))
            {
                ((M_EndUsers_ViewModel)this.DataContext).My_M_WorkOrders_ViewModel.LoadApproved();
            }
            else
            {
                ((M_EndUsers_ViewModel)this.DataContext).My_M_WorkOrders_ViewModel.LoadApplications();
            }

            //ObservableCollection<tbl_eusr_tmp> mylst = ((M_EndUsers_ViewModel)this.DataContext).lstUser_Model;
            //ObservableCollection<tbl_eusr_tmp> mylst2;

            //string search = ((M_EndUsers_ViewModel)this.DataContext).Search;

            //if (!string.IsNullOrEmpty(search))
            //{
            //    //mylst2 = (ObservableCollection<tbl_eusr_tmp>)mylst.Where(x => x.Name.Contains(search, StringComparison.InvariantCultureIgnoreCase));
            //    mylst2 = mylst.SelectMany(x => x.);
            //}
            //else
            //{
            //    ((M_EndUsers_ViewModel)this.DataContext).lstUser_Model_Filtered = ((M_EndUsers_ViewModel)this.DataContext).lstUser_Model;
            //}
        }

        private void Check_Clicked(object sender, RoutedEventArgs e)
        {

        }

        private void docPanel_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void Users_Rows_Selected(object sender, SelectionChangedEventArgs e)
        {
            if (gridUsers.SelectedItems.Count == 1)
            {
                ((M_EndUsers_ViewModel)this.DataContext).My_M_WorkOrders_ViewModel.SelectedWorkOrder = (UIMWorkOrder_detail_response_simple)gridUsers.SelectedItem;
            }
        }
    }
}
