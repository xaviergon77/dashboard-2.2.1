﻿using ExchangeDashboard.ViewModels;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CefSharp;
using CefSharp.Wpf;

namespace ExchangeDashboard.Views
{
    /// <summary>
    /// Interaction logic for MM_ZoomImage.xaml
    /// </summary>
    public partial class MM_ZoomPdf : Window
    {
        public Bitmap image;
        public MM_ZoomPdf()
        {
            InitializeComponent();
            Loaded += Page_Loaded;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //var settings = new CefSettings();
            ////settings.EnableInternalPdfViewerOffScreen();
            //// Disable GPU in WPF and Offscreen examples until #1634 has been resolved
            //settings.CefCommandLineArgs.Add("disable-gpu", "1");
            //settings.CachePath = "cache";

            ////Cef.Initialize(settings, ShutdownMode: true, performDependencyCheck: true);
            //Cef.Initialize(settings, ShutdownMode.OnLastWindowClose, DependencyChecker.CheckDependencies);


            ////webwindow.NavigateToString(@"<HTML><IFRAME SCROLLING=""YES"" SRC=""" + ((MM_ZoomPdf_ViewModel)this.DataContext).Path + @""" ></IFRAME></HTML>");
            ////webwindow.Navigate(new Uri(((MM_ZoomPdf_ViewModel)this.DataContext).Path));
            ////webwindow.Navigate(new System.Uri(((MM_ZoomPdf_ViewModel)this.DataContext).Path));

            ////webwindow.Address =(@"file:///" + ((MM_ZoomPdf_ViewModel)this.DataContext).Path);
        }


    }
}
