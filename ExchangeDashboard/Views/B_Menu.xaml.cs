﻿using ExchangeDashboard.Lib;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExchangeDashboard
{
    /// <summary>
    /// Interaction logic for B_Menu.xaml
    /// </summary>
    public partial class B_Menu : Page
    {
        public B_Menu()
        {
            InitializeComponent();
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Render, new Action(() =>
            //{
            //    var navWindow = Window.GetWindow(this) as NavigationWindow;
            //    if (navWindow != null) navWindow.ShowsNavigationUI = false;
            //}));
        }

        private void GoSettings(object sender, RoutedEventArgs e)
        {

        }

        private void GoOperations(object sender, RoutedEventArgs e)
        {

        }

        private void GoReports(object sender, RoutedEventArgs e)
        {

        }

        private void GoUsers(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Views/M_AdminUsers.xaml");
        }
        private void GoWorkOrder(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Views/M_WorkOrders.xaml");
        }

        private void GoEndUsers(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Views/M_EndUsers.xaml");
        }
    }
}
