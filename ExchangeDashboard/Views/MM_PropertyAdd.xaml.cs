﻿using ExchangeDashboard.ViewModels;
using ExchangeDB;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExchangeDashboard
{
    /// <summary>
    /// Interaction logic for M_PropertyAdd.xaml
    /// </summary>
    public partial class MM_PropertyAdd : Window
    {
        public MM_PropertyAdd()
        {
            InitializeComponent();
            Loaded += Window_Loaded;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ((MM_UserProperty_ViewModel)this.DataContext).LoadProperties();
            ((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty = null;
        }


        /*
         * Check the property instance to return, 
         * it should be a "tbl_property_val" object
         */
        public CAT_property Result()
        {
            return ((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty;
        }

        private void txtValue_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

        }

        private void cmbProperty_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty == null) return;

            //((MM_UserProperty_ViewModel)this.DataContext).PropertyVal = new tbl_property_val()
            //{
            //    ActionName = ((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty.CAT_action_Name,
            //    Behavior = ((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty.Behavior,
            //    CAT_action_Id = ((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty.CAT_action_Id,
            //    SourceUri = "addproperty.form",
            //    Type = ((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty.Type,
            //    CAT_property_Id = ((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty.Id,
            //    IsVerifiable = ((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty.IsVerifiable,
            //    Message = "",
            //    Name = ((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty.Name,
            //    Notes = "",
            //    RegExp = ((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty.RegExp,
            //    Value = "",
            //    StepName = ((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty.CAT_step_Name,
            //    DateCreated = DateTime.Now,
            //    CAT_schema_object_Id = ((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty.CAT_schema_object_Id,
            //    CAT_step_Id = ((MM_UserProperty_ViewModel)this.DataContext).SelectedProperty.CAT_step_Id,
            //    CAT_usr_gen_Id = 99,
            //    CAT_usr_gen_Id_Created = 98
            //};
        }
    }
}
