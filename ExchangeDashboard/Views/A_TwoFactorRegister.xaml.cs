﻿using DAI.CORE.UIM.UIM_QR_Dash;
using ExchangeDashboard.Lib;
using ExchangeDashboard.ViewModels;
using Google_Authenticator_netcore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExchangeDashboard
{
    /// <summary>
    /// Interaction logic for A_TwoFactorRegister.xaml
    /// </summary>
    public partial class A_TwoFactorRegister : Page
    {

        public A_TwoFactorRegister()
        {
            InitializeComponent();
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            ((A_TwoFactorRegister_ViewModel)this.DataContext).Cmd_A_TwoFactor_GetQRToken.Execute(null);

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Navigator.Navigate("Views/A_Login.xaml");
        }
    }
}
