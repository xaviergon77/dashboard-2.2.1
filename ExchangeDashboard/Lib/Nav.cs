﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Navigation;

namespace ExchangeDashboard.Lib
{
    public static class Navigator
    {
        private static object _data = null;

        private static NavigationService NavigationService { get; } = (Application.Current.MainWindow as A_MainWindow).MainFrame.NavigationService;
        public static void Navigate(string path, object param = null)
        {
            NavigationService.Navigate(new Uri(path, UriKind.RelativeOrAbsolute), param);
        }

        public static void NavigateWithState(string path, object param = null)
        {
            _data = param;
            NavigationService.Navigate(new Uri(path, UriKind.RelativeOrAbsolute));
        }

        public static object GetLastNavigationData(this NavigationService service)
        {
            object data = _data;
            _data = null;
            return data;
        }

        public static void GoBack()
        {
            NavigationService.GoBack();
        }

        public static void GoForward()
        {
            NavigationService.GoForward();
        }
    }

}
