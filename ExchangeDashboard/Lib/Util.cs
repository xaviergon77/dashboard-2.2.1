﻿using DAI.CORE.UIM.UIM_StaticInfo;
using Microsoft.Extensions.Configuration;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Windows;

namespace ExchangeDashboard.Lib
{
    public class Util
    {

        public static string GetSourceUri()
        {
            return "uri:" +
                "exc:" +
                "1:" +
                "res:" +
                "cr:" +
                "dashboard:" +
                System.Windows.Application.Current.Properties["UserName"] + ":" +
                System.Windows.Application.Current.Properties["UserIp"] + ":" +
                "app:" +
                "0";
        }

        public static IConfiguration GetConfiguration()
        {
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile(@"appsettings.json", optional: false);
            return builder.Build();
        }

        public static string GetProperty(string p)
        {
            return System.Windows.Application.Current.Properties[p].ToString();
        }

        public static void ClearApplicationProperties()
        {
            Application.Current.Properties.Remove("UserId");
            Application.Current.Properties.Remove("UserName");
            Application.Current.Properties.Remove("UserIp");
            Application.Current.Properties["AppName"] = "Exchange Dashboard";
            Application.Current.Properties.Remove("IsTwoFactorAuth");
            Application.Current.Properties.Remove("IsGoogleAuth");
            Application.Current.Properties.Remove("Authenticated");
        }

        public static void LogOff()
        {
            ClearApplicationProperties();
            ((A_MainWindow)App.Current.MainWindow).MainFrame.Source = new Uri("A_Login.xaml", UriKind.RelativeOrAbsolute);
        }

        public static void CheckIfAuthenticated()
        {
            if ((bool)Application.Current.Properties["Authenticated"] != true)
            {
                LogOff();
            }
        }


 

    }
}
