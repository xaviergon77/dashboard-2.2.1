﻿using ExchangeDashboard.Commands;
using ExchangeDB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ExchangeDashboard.ViewModels
{
    internal class MM_UserProperty_ViewModel : INotifyPropertyChanged
    {
        public MM_UserProperty_ViewModel()
        {
            MM_UserProperty_Some_Command = new UserProperty_Some_Command(this);
        }

        public ICommand MM_UserProperty_Some_Command
        {
            get;
            private set;
        }

        //private tbl_property_val _PropertyVal;
        //public tbl_property_val PropertyVal
        //{
        //    set
        //    {
        //        _PropertyVal = value;
        //        OnPropertyChanged("PropertyVal");
        //    }
        //    get
        //    {
        //        return _PropertyVal;
        //    }
        //}

        private CAT_property _SelectedProperty;
        public CAT_property SelectedProperty
        {
            set
            {
                _SelectedProperty = value;
                OnPropertyChanged("SelectedProperty");
            }
            get
            {
                return _SelectedProperty;
            }
        }


        private ObservableCollection<CAT_property> _lstProperties;
        public ObservableCollection<CAT_property> lstProperties
        {
            set
            {
                _lstProperties = value;
                OnPropertyChanged("lstProperties");
            }
            get
            {
                return _lstProperties;
            }
        }

        public void LoadProperties()
        {
        //    lstProperties = new ObservableCollection<CAT_property>() { };

        //    lstProperties.Add(new CAT_property()
        //    {
        //        Name = "Select a Property",
        //        CAT_action_Id = 0,
        //        CAT_action_Name=null,
        //        Behavior = null,
        //        Id = 0,
        //        CAT_schema_object_Id = 0,
        //        IsVerifiable = false,
        //        Description = null,
        //        IsTenant = true,
        //        CAT_step_Id = 0,
        //        CAT_step_Name = null,
        //        Type = null,
        //        RegExp = null
        //    });

        //    lstProperties.Add(new CAT_property()
        //    {
        //        Name = "CUSTOM PROPERTY",
        //        CAT_action_Id = 0,
        //        CAT_action_Name = null,
        //        Behavior = null,
        //        Id = 0,
        //        CAT_schema_object_Id = 0,
        //        IsVerifiable = false,
        //        Description = null,
        //        IsTenant = true,
        //        CAT_step_Id = 0,
        //        CAT_step_Name = null,
        //        Type = null,
        //        RegExp = null
        //    });

        //    lstProperties.Add(new CAT_property()
        //    {
        //        Name = "propiedad 1",
        //        CAT_action_Id = 2,
        //        CAT_action_Name = "ApplicatioReview",
        //        Behavior = "None",
        //        Id = 1,
        //        CAT_schema_object_Id = 1,
        //        IsVerifiable = true,
        //        Description = "sample property",
        //        IsTenant = true,
        //        CAT_step_Id = 25,
        //        CAT_step_Name = "Application",
        //        Type = "Text",
        //        RegExp = @"[a-zA-Z0-9,\ \-\_\'\.\""\#\$]*"
        //    });

        //    lstProperties.Add(new CAT_property()
        //    {
        //        Name = "propiedad 2",
        //        CAT_action_Id = 2,
        //        CAT_action_Name = "ApplicatioReview",
        //        Behavior = "None",
        //        Id = 2,
        //        CAT_schema_object_Id = 1,
        //        IsVerifiable = true,
        //        Description = "sample property 2",
        //        IsTenant = true,
        //        CAT_step_Id = 25,
        //        CAT_step_Name = "Application",
        //        Type = "Text",
        //        RegExp = @"[a-zA-Z0-9,\ \-\_\'\.\""\#\$]*"
        //    });

        //    BitmapImage logo = new BitmapImage();
        //    logo.BeginInit();
        //    logo.UriSource = new Uri(@"E:\WORK\RESIST\DASHBOARD\ExchangeDashboard\Resources\qr.PNG", UriKind.RelativeOrAbsolute);
        //    logo.EndInit();

        //    BitmapImage signature = new BitmapImage();
        //    signature.BeginInit();
        //    signature.UriSource = new Uri(@"E:\WORK\RESIST\DASHBOARD\ExchangeDashboard\Resources\qr.PNG", UriKind.RelativeOrAbsolute);
        //    signature.EndInit();

        //    if (PropertyVal == null) PropertyVal = new tbl_property_val();

        //    SelectedProperty.PropertyChanged += (s, e) => {
        //        switch (e.PropertyName)
        //        {
        //            case "Id":
        //                PropertyVal.CAT_property_Id = SelectedProperty.Id;
        //                break;
        //            case "CAT_schema_object_Id":
        //                PropertyVal.CAT_schema_object_Id = SelectedProperty.CAT_schema_object_Id;
        //                break;
        //            case "CAT_step_Id":
        //                PropertyVal.CAT_step_Id = SelectedProperty.CAT_step_Id;
        //                break;
        //            case "CAT_step_Name":
        //                PropertyVal.StepName = SelectedProperty.CAT_step_Name;
        //                break;
        //            case "CAT_action_Id":
        //                PropertyVal.CAT_action_Id = SelectedProperty.CAT_action_Id;
        //                break;
        //            case "CAT_action_Name":
        //                PropertyVal.ActionName = SelectedProperty.CAT_action_Name;
        //                break;
        //            case "Type":
        //                PropertyVal.Type = SelectedProperty.Type;
        //                break;
        //            case "Name":
        //                PropertyVal.Name = SelectedProperty.Name;
        //                break;
        //            case "RegExp":
        //                PropertyVal.RegExp = SelectedProperty.RegExp;
        //                break;
        //            case "Behavior":
        //                PropertyVal.Behavior = SelectedProperty.Behavior;
        //                break;
        //        }
        //    };
        }

        #region INotifyPropertyChangedMembers
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }

            //switch (propertyName)
            //{
            //    case "TipoArchivo":
            //        switch (TipoArchivo)
            //        {
            //            case "INSTANT10DE15REALTIME":
            //                this.Archivo = new ArchivoInstant10De15RealTime();
            //                break;
            //            case "3GRANDES":
            //                this.Archivo = new Archivo3Grandes();
            //                break;
            //            case "CHANCE80":
            //                this.Archivo = new ArchivoChance80();
            //                break;
            //            case "CHANCE100":
            //                this.Archivo = new ArchivoChance100();
            //                break;
            //            case "INSTANT3DE6":
            //                this.Archivo = new ArchivoInstant3De6();
            //                break;
            //            case "TIEMPOS":
            //                this.Archivo = new ArchivoTiempos();
            //                break;
            //            case "NACIONAL":
            //                this.Archivo = new ArchivoNacional();
            //                break;
            //            case "POPULAR":
            //                this.Archivo = new ArchivoPopular();
            //                break;
            //        }
            //        break;
            //}
        }
        #endregion

    }
}
