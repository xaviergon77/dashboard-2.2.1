﻿using ExchangeDashboard.Commands;
using ExchangeDB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ExchangeDashboard.ViewModels
{
    internal class MM_ZoomImage_ViewModel : ExchangeDB.ViewModelBase
    {
        public MM_ZoomImage_ViewModel()
        {
            MM_ZoomImage_Some_Command = new ZoomImage_Some_Command(this);
        }

        public ICommand MM_ZoomImage_Some_Command
        {
            get;
            private set;
        }

        private Bitmap _Image;
        public Bitmap Image
        {
            set
            {
                _Image = value;
                OnPropertyChanged("Image");
            }
            get
            {
                return _Image;
            }
        }

        private string _Path;
        public string Path
        {
            set
            {
                _Path = value;
                OnPropertyChanged("Path");
                Bitmap img = new Bitmap(value);
                Image = img;
            }
            get
            {
                return _Path;
            }
        }
    }
}
