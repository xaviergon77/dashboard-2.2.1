﻿using ExchangeDashboard.Commands;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ExchangeDashboard.ViewModels
{
    internal class StatusBar_ViewModel : ExchangeDB.ViewModelBase
    {
        public StatusBar_ViewModel()
        {
            M_EndUsers.StatusMessage += NewMessageHandler;
            StatusBar_Command = new StatusBar_Command(this);
        }

        public ICommand StatusBar_Command
        {
            get;
            private set;
        }

        private string _Message;
        public string Message
        {
            set
            {
                if (value == _Message) return;
                _Message = value;
                OnPropertyChanged("Message");
            }
            get
            {
                return _Message;
            }
        }

        public async Task ShowMessageAndHide(string message)
        {
            Message = message;
            await Task.Delay(7000);
            Message = string.Empty;
        }

        private async void NewMessageHandler(object sender, EventArgs e)
        {
            await Task.Run(() =>
            {
                ShowMessageAndHide(sender.ToString());
            });
        }


    }
}
