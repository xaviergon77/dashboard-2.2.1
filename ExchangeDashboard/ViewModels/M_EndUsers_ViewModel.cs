﻿using ExchangeDashboard.Commands;
using ExchangeDB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Drawing;
using DAI.CORE.UIM.UIM_StaticInfo;
using System.IO;
using System.Linq;
using DAI.CORE.UIM.UIM_WorkOrder;
using DAI.CORE.UIM.UIM_BalanceDash;
using DAI.CORE.UIM.UIM_CoreRegEu;
using DAI.CORE.UIM.UIM_PropertyVal;
using DAI.CORE.UIM.ImagesEu;
using System.Windows;

namespace ExchangeDashboard.ViewModels
{
    internal class M_EndUsers_ViewModel : Base_ViewModel
    {
        //public int paramId = 0;

        //public M_EndUsers_ViewModel(UIMWorkOrder_detail_response WorkOrder)
        public M_EndUsers_ViewModel()
        {
            My_M_WorkOrders_ViewModel = new M_WorkOrders_ViewModel();
            //CMD_M_EndUsers_LoadUser = new M_EndUsers_LoadSelectedUsr(this);
        }

        private M_WorkOrders_ViewModel _My_M_WorkOrders_ViewModel { get; set; }
        public M_WorkOrders_ViewModel My_M_WorkOrders_ViewModel
        {
            set
            {
                _My_M_WorkOrders_ViewModel = value;
                OnPropertyChanged("My_M_WorkOrders_ViewModel");
            }
            get
            {
                return _My_M_WorkOrders_ViewModel;
            }
        }

        private ICommand _CMD_M_EndUsers_LoadUser { get; set; }
        public ICommand CMD_M_EndUsers_LoadUser
        {
            get
            {
                return _CMD_M_EndUsers_LoadUser ??
                (_CMD_M_EndUsers_LoadUser =
                new LoadSelectedUsr_Command(
                    () => LoadUserAction(),
                    () => LoadUserCanExecute,
                    this
                    )
                );
            }
        }

        public bool LoadUserCanExecute
        {
            get
            {
                return true;
            }
        }

        public void LoadUserAction()
        {
            //do something here
        }

        private ICommand _CMD_M_EndUser_Approve;
        public ICommand CMD_M_EndUser_Approve
        {
            get
            {
                return _CMD_M_EndUser_Approve ??
                (_CMD_M_EndUser_Approve =
                new Approve_Application_Command(
                    this
                    )
                );
            }
        }
        
        
        private ICommand _CMD_M_EndUser_GetBalance;
        public ICommand CMD_M_EndUser_GetBalance
        {
            get
            {
                return _CMD_M_EndUser_GetBalance ??
                (_CMD_M_EndUser_GetBalance =
                new GetBalances_Command(
                    this
                    )
                );
            }
        }

        private bool _ApprovedChecked;
        public bool ApprovedChecked
        {
            set
            {
                _ApprovedChecked = value;
                OnPropertyChanged("ApprovedChecked");
            }
            get
            {
                return _ApprovedChecked;
            }
        }

        private string _SelectedFilter;
        public string SelectedFilter
        {
            set
            {
                _SelectedFilter = value;
                OnPropertyChanged("SelectedFilter");
                OnPropertyChanged("TitleBg");
                OnPropertyChanged("TitleFg");
                OnPropertyChanged("VisibilityBalance");
                OnPropertyChanged("HeightChecknMeta");
            }
            get
            {
                return _SelectedFilter;
            }
        }

        private string _Search;
        public string Search
        {
            set
            {
                _Search = value;
                OnPropertyChanged("Search");
                lstUser_Model_Filtered = lstUser_Model;
            }
            get
            {
                return _Search?.Trim();
            }
        }

        public string TitleBg
        {
            get
            {
                if (SelectedFilter == "Specific Approved User" || SelectedFilter == "All Approved Users")
                {
                    return "Bisque";
                }
                else
                {
                    return "Red";
                }
            }
        }
        public string TitleFg
        {
            get
            {
                if (SelectedFilter == "Specific Approved User" || SelectedFilter == "All Approved Users")
                {
                    return "Black";
                }
                else
                {
                    return "White";
                }
            }
        }



        //private UIMCountry_detail _SelectedCountryPassport;
        //public UIMCountry_detail SelectedCountryPassport
        //{
        //    set
        //    {
        //        _SelectedCountryPassport = value;
        //        OnPropertyChanged("SelectedCountryPassport");
        //    }
        //    get
        //    {
        //        return _SelectedCountryPassport;
        //    }
        //}
        //private UIMCountry_detail _SelectedCountrySecondId;
        //public UIMCountry_detail SelectedCountrySecondId
        //{
        //    set
        //    {
        //        _SelectedCountrySecondId = value;
        //        OnPropertyChanged("SelectedCountrySecondId");
        //    }
        //    get
        //    {
        //        return _SelectedCountrySecondId;
        //    }
        //}

        //private UIMCountry_detail _SelectedCountryCitizen;
        //public UIMCountry_detail SelectedCountryCitizen
        //{
        //    set
        //    {
        //        _SelectedCountryCitizen = value;
        //        OnPropertyChanged("SelectedCountryCitizen");
        //    }
        //    get
        //    {
        //        return _SelectedCountryCitizen;
        //    }
        //}
        //private UIMCountry_detail _SelectedCountryResident;
        //public UIMCountry_detail SelectedCountryResident
        //{
        //    set
        //    {
        //        _SelectedCountryResident = value;
        //        OnPropertyChanged("SelectedCountryResident");
        //    }
        //    get
        //    {
        //        return _SelectedCountryResident;
        //    }
        //}

        //private string _SelectedTypeSecondId;
        //public string SelectedTypeSecondId
        //{
        //    set
        //    {
        //        _SelectedTypeSecondId = value;
        //        OnPropertyChanged("SelectedTypeSecondId");
        //    }
        //    get
        //    {
        //        return _SelectedTypeSecondId;
        //    }
        //}
        //private string _SelectedStatusMarital;
        //public string SelectedStatusMarital
        //{
        //    set
        //    {
        //        _SelectedStatusMarital = value;
        //        OnPropertyChanged("SelectedStatusMarital");
        //    }
        //    get
        //    {
        //        return _SelectedStatusMarital;
        //    }
        //}

        private UIMWorkOrder_detail_response_simple _SelectedUser;
        public UIMWorkOrder_detail_response_simple SelectedUser
        {
            set
            {
                _SelectedUser = value;
                CMD_M_EndUser_GetBalance.Execute(this);
                OnPropertyChanged("SelectedUser");
            }
            get
            {
                return _SelectedUser;
            }
        }

        private UIMCoreRegEu_detail_response _SelectedCoreAndReg;
        public UIMCoreRegEu_detail_response SelectedCoreAndReg
        {
            set
            {
                _SelectedCoreAndReg = value;
                OnPropertyChanged("SelectedCoreAndReg");
            }
            get
            {
                return _SelectedCoreAndReg;
            }
        }

        private ObservableCollection<UIMPropertyValWo_detail_response> _lstChecks;
        public ObservableCollection<UIMPropertyValWo_detail_response> lstChecks
        {
            set
            {
                _lstChecks = value;
                OnPropertyChanged("lstChecks");
            }
            get
            {
                return _lstChecks;
            }
        }

        private UIMImagesEu_detail_response[] _arrDocuments;
        public UIMImagesEu_detail_response[] arrDocuments
        {
            set
            {
                _arrDocuments = value;
                OnPropertyChanged("arrDocuments");
            }
            get
            {
                return _arrDocuments;
            }
        }

        private ObservableCollection<UIMWorkOrder_detail_response_simple> _lstUser_Model;
        public ObservableCollection<UIMWorkOrder_detail_response_simple> lstUser_Model
        {
            set
            {
                _lstUser_Model = value;
                OnPropertyChanged("lstUser_Model");
            }
            get
            {
                return _lstUser_Model;
            }
        }

        private ObservableCollection<UIMWorkOrder_detail_response_simple> _lstUser_Model_Filtered;
        public ObservableCollection<UIMWorkOrder_detail_response_simple> lstUser_Model_Filtered
        {
            set
            {
                if (!string.IsNullOrEmpty(_Search))
                {
                    _lstUser_Model_Filtered = new ObservableCollection<UIMWorkOrder_detail_response_simple>() { };
                    foreach (UIMWorkOrder_detail_response_simple item in value)
                    {
                        if (item.firstName.Contains(_Search, StringComparison.InvariantCultureIgnoreCase))
                        {
                            _lstUser_Model_Filtered.Add(item);
                        }
                    }
                }
                else
                {
                    _lstUser_Model_Filtered = value;
                }
                OnPropertyChanged("lstUser_Model_Filtered");
            }
            get
            {
                return _lstUser_Model_Filtered;
            }
        }

        public void LoadUsers(UIMWorkOrder_detail_response_simple workorder)
        {
            lstUser_Model = new ObservableCollection<UIMWorkOrder_detail_response_simple>() { };
            if (workorder == null)
            {
                lstUser_Model_Filtered = _My_M_WorkOrders_ViewModel.lstWork_Order;
                //do nothing for now, it's taking now My_M_WorkOrders_ViewModel.lstWork_Order
            }
            else
            {
                lstUser_Model.Add(workorder);
                SelectedUser = workorder;
                lstUser_Model_Filtered = lstUser_Model;
            }

            /*
            //lstUser_Model.Add(new UIMWorkOrder_detail_response()
            //{
            //    Id = 34,
            //    CAT_usr_gen_Id = 922019200999,
            //    Name = "Jose A Gomez",
            //    IsActive = true,
            //    Loc2 = "MX",
            //    Usr = "MX12345XU20Z2",
            //    Docs = new ObservableCollection<GridItemViewModel>() { },
            //    Registration = new tbl_usr_reg()
            //    {
            //        FirstName = "Jose",
            //        SecondName = "A",
            //        LastName = "Gomez",
            //         HomePhone = "+321 123491283741892"
            //    }
            //});
            //lstUser_Model.Add(new tbl_eusr_tmp()
            //{
            //    Id = 25,
            //    CAT_usr_gen_Id=2938274387,
            //    Name = "Maria Avendas",
            //    IsActive = true,
            //    Loc2 = "NI",
            //    Usr = "NI12345XU20Z2",
            //    Docs = new ObservableCollection<GridItemViewModel>() { },
            //    Registration = new tbl_usr_reg()
            //    {
            //        FirstName = "Maria",
            //        SecondName = "",
            //        LastName = "Avendas",
            //        HomePhone = "+928 292382929298734"
            //    }
            //});

            //string pathDocuments = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            //string pathPassportGirl = Path.Combine(pathDocuments, "ExchangeParlor", "temporary", "passport_girl.jpg");
            //string pathPassportGuy = Path.Combine(pathDocuments, "ExchangeParlor", "temporary", "passport_guy.jpg");
            //string pathLicenseGirl = Path.Combine(pathDocuments, "ExchangeParlor", "temporary", "license_girl.jpg");
            //string PathPdfGirl = Path.Combine(pathDocuments, "ExchangeParlor", "temporary", "made-with-cc.pdf");
            //string pathLicenseGuy = Path.Combine(pathDocuments, "ExchangeParlor", "temporary", "licence_guy.jpg");
            //string pathSignatureGirl = Path.Combine(pathDocuments, "ExchangeParlor", "temporary", "Signature-Pad.png");
            //string pathSignatureGuy = Path.Combine(pathDocuments, "ExchangeParlor", "temporary", "Signature_example.png");

            //Bitmap passport_girl = new Bitmap(pathPassportGirl);
            //Bitmap passport_guy = new Bitmap(pathPassportGuy);
            //Bitmap license_girl = new Bitmap(pathLicenseGirl);
            //Bitmap license_guy = new Bitmap(pathLicenseGuy);
            //Bitmap signature_girl = new Bitmap(pathSignatureGirl);
            //Bitmap signature_guy = new Bitmap(pathSignatureGuy);

            //lstUser_Model[0].Docs.Add(new GridItemViewModel { Title = "Passport", Description = "Government Id", ImageSource = passport_guy, Path = pathPassportGuy });
            //lstUser_Model[0].Docs.Add(new GridItemViewModel { Title = "License", Description = "Driving Id", ImageSource = license_guy, Path = pathLicenseGuy });
            //lstUser_Model[0].Signature = signature_guy;

            //lstUser_Model[1].Docs.Add(new GridItemViewModel { Title = "Passport", Description = "Government Id", ImageSource = passport_girl, Path = pathPassportGirl });
            //lstUser_Model[1].Docs.Add(new GridItemViewModel { Title = "License", Description = "Driving Id", ImageSource = license_girl, Path = pathLicenseGirl });
            //lstUser_Model[1].Docs.Add(new GridItemViewModel { Title = "Resume", Description = "Resume", ImageSource = null, Path = PathPdfGirl });
            //lstUser_Model[1].Signature = signature_girl;
             */


        }
    }
}
