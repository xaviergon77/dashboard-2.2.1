﻿using ExchangeDashboard.Commands;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ExchangeDashboard.ViewModels
{
    internal class A_MainWindow_ViewModel : ExchangeDB.ViewModelBase
    {
        public A_MainWindow_ViewModel()
        {
        }

        private string _Title { get; set; }

        public string Title
        {
            set
            {
                _Title = value;
                OnPropertyChanged("Title");
            }
            get
            {
                return _Title;
            }
        }
    }
}
