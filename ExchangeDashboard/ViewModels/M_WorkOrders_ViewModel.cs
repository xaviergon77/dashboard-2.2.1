﻿using DAI.CORE.UIM.ImagesEu;
using DAI.CORE.UIM.UIM_BalanceDash;
using DAI.CORE.UIM.UIM_CoreRegEu;
using DAI.CORE.UIM.UIM_PropertyVal;
using DAI.CORE.UIM.UIM_WorkOrder;
using ExchangeDashboard.Commands;
using ExchangeDB;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ExchangeDashboard.ViewModels
{
    internal class M_WorkOrders_ViewModel : ViewModelBase
    {
        public M_WorkOrders_ViewModel()
        {
            BalanceVisibility = Visibility.Collapsed;
            ChecksVisibility = Visibility.Collapsed;
            HeightChecknMeta = "Auto";
            new GetWorkOrders(this).Execute(null);
        }

        public void LoadApproved()
        {
            new GetWorkOrdersPrd_Command(this).Execute(null);
        }

        public void LoadApplications()
        {
            BalanceVisibility = Visibility.Collapsed;
            ChecksVisibility = Visibility.Collapsed;
            HeightChecknMeta = "Auto";
            new GetWorkOrders(this).Execute(null);
        }


        private IConfiguration _Configuration { get; set; }
        private ICommand _CMD_M_WorkOrders_Load_Command;
        public ICommand CMD_M_WorkOrders_Load_Command
        {
            get
            {
                return _CMD_M_WorkOrders_Load_Command ??
                (_CMD_M_WorkOrders_Load_Command =
                new WorkOrders_Load_Command(
                    () => MyAction(),
                    () => CanExecute,
                    this
                    )
                );
            }
        }

        public bool CanExecute
        {
            get
            {
                return true;
            }
        }

        public void MyAction()
        {
            _Configuration = Lib.Util.GetConfiguration();

            UIMWorkOrder_request r = new UIMWorkOrder_request()
            {
                sourceuri = Lib.Util.GetSourceUri(),
            };

            UIMWorkOrder_response a = DAI.CORE.Manager.WorkOrder.WorkOrderLogicServices.LoadData(_Configuration, r);

            if (((UIMWorkOrder_response)a).result == true)
            {
                lstWork_Order = new ObservableCollection<UIMWorkOrder_detail_response_simple>(a.data);
            }
        }

        private ObservableCollection<UIMWorkOrder_detail_response_simple> _lstUser_Model;
        public ObservableCollection<UIMWorkOrder_detail_response_simple> lstUser_Model
        {
            set
            {
                _lstUser_Model = value;
                OnPropertyChanged("lstUser_Model");
            }
            get
            {
                return _lstUser_Model;
            }
        }


        private ObservableCollection<UIMWorkOrder_detail_response_simple> _lstUser_Model_Filtered;
        public ObservableCollection<UIMWorkOrder_detail_response_simple> lstUser_Model_Filtered
        {
            set
            {
                if (!string.IsNullOrEmpty(_Search))
                {
                    _lstUser_Model_Filtered = new ObservableCollection<UIMWorkOrder_detail_response_simple>() { };
                    foreach (UIMWorkOrder_detail_response_simple item in value)
                    {
                        if (item.firstName.Contains(_Search, StringComparison.InvariantCultureIgnoreCase))
                        {
                            _lstUser_Model_Filtered.Add(item);
                        }
                    }
                }
                else
                {
                    _lstUser_Model_Filtered = value;
                }
                OnPropertyChanged("lstUser_Model_Filtered");
            }
            get
            {
                return _lstUser_Model_Filtered;
            }
        }

        private string _Search;
        public string Search
        {
            set
            {
                _Search = value;
                OnPropertyChanged("Search");
                lstUser_Model_Filtered = lstUser_Model;
            }
            get
            {
                return _Search?.Trim();
            }
        }


        //private ICommand _CMD_M_WorkOrders_View_Command;
        //public ICommand CMD_M_WorkOrders_View_Command
        //{
        //    get
        //    {
        //        //return _CMD_M_WorkOrders_View_Command ??
        //        //(_CMD_M_WorkOrders_View_Command =
        //        //new M_WorkOrders_View_Command(
        //        //    () => ViewAction(),
        //        //    () => CanExecuteView,
        //        //    this
        //        //    )
        //        //);
        //        return new RelayCommand<UIMWorkOrder_detail_response>(M_EndUsers_LoadSelectedUsr);
        //    }
        //}

        //public bool CanExecuteView
        //{
        //    get
        //    {
        //        return SelectedWorkOrder != null;
        //    }
        //}

        //public void ViewAction()
        //{
        //    M_EndUsers page2 = new M_EndUsers();
        //    page2.paramId = ((M_WorkOrders_ViewModel)this.DataContext).SelectedWorkOrder.caT_usr_gen_Id;
        //    page2.paramIsApplications = true;
        //    this.NavigationService.Navigate(page2);

        //}









        public ICommand M_EndUsers_Some_Command
        {
            get;
            private set;
        }

        private UIMWorkOrder_detail_response_simple _SelectedWorkOrder;
        public UIMWorkOrder_detail_response_simple SelectedWorkOrder
        {
            set
            {
                _SelectedWorkOrder = value;
                OnPropertyChanged("SelectedWorkOrder");
                new GetChecks_Command(this).Execute(null);
                new GetUserCoreAndReg_Command(this).Execute(null);
                //new GetDocuments_CommandAsync(this).ExecuteAsync();
                new GetDocuments_Command(this).Execute(null);
                new GetBalancesWo_Command(this).Execute(null);
            }
            get
            {
                return _SelectedWorkOrder;
            }
        }

        private UIMBalanceDash_detail_response[] _Balances;
        public UIMBalanceDash_detail_response[] Balances
        {
            get
            {
                return _Balances;
            }
            set
            {
                _Balances = value;
                OnPropertyChanged("Balances");
                BalanceVisibility = value.Length > 0 ? Visibility.Visible : Visibility.Collapsed;
                HeightChecknMeta = BalanceVisibility == Visibility.Visible ? "300" : "auto";
            }
        }


        public string _HeightChecknMeta { get; set; }

        public string HeightChecknMeta
        {
            set
            {
                _HeightChecknMeta = value;
                OnPropertyChanged("HeightChecknMeta");
            }
            get
            {
                return _HeightChecknMeta;
            }
        }

        private Visibility _BalanceVisibility { get; set; }
        public Visibility BalanceVisibility
        {
            set
            {
                _BalanceVisibility = value;
                OnPropertyChanged("BalanceVisibility");
            }
            get
            {
                return _BalanceVisibility;
            }
        }

        private Visibility _ChecksVisibility { get; set; }

        public Visibility ChecksVisibility
        {
            set
            {
                _ChecksVisibility = value;
                OnPropertyChanged("ChecksVisibility");
            }
            get
            {
                return _ChecksVisibility;
            }
        }

        private ObservableCollection<UIMPropertyValWo_detail_response> _SelectedChecks;
        public ObservableCollection<UIMPropertyValWo_detail_response> SelectedChecks
        {
            set
            {
                _SelectedChecks = value;
                OnPropertyChanged("SelectedChecks");
                ChecksVisibility = value?.Count > 0 ? Visibility.Visible : Visibility.Collapsed;
            }
            get
            {
                return _SelectedChecks;
            }
        }

        private ObservableCollection<UIMImagesEu_detail_response> _SelectedDocuments;
        public ObservableCollection<UIMImagesEu_detail_response> SelectedDocuments
        {
            set
            {
                _SelectedDocuments = value;
                if (value?.Count > 0)
                {
                    try
                    {
                        if (System.IO.Directory.Exists(value[0].localpath))
                        {
                            System.IO.DirectoryInfo di = new DirectoryInfo(value[0].localpath);
                            foreach (FileInfo file in di.GetFiles())
                            {
                                file.Delete();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //do nothing
                    }

                    foreach (UIMImagesEu_detail_response item in value)
                    {
                        ExchangeTools.ImageUtils.SaveImage(item.localfilepath, item.imageBitmap);
                    }
                }
                OnPropertyChanged("SelectedDocuments");
            }
            get
            {
                return _SelectedDocuments;
            }
        }

        private UIMCoreRegEu_detail_response _SelectedRegAndCore;
        public UIMCoreRegEu_detail_response SelectedRegAndCore
        {
            set
            {
                _SelectedRegAndCore = value;
                OnPropertyChanged("SelectedRegAndCore");
            }
            get
            {
                return _SelectedRegAndCore;
            }
        }

        private ObservableCollection<UIMWorkOrder_detail_response_simple> _lstWork_Order;
        public ObservableCollection<UIMWorkOrder_detail_response_simple> lstWork_Order
        {
            set
            {
                _lstWork_Order = value;
                OnPropertyChanged("lstWork_Order");
                lstUser_Model_Filtered = _lstWork_Order;
            }
            get
            {
                return _lstWork_Order;
            }
        }

        private ObservableCollection<UIMWorkOrder_detail_response_simple> _List_Work_Orders;
        public ObservableCollection<UIMWorkOrder_detail_response_simple> List_Work_Orders
        {
            set
            {
                _List_Work_Orders = value;
                OnPropertyChanged("List_Work_Orders");
            }
            get
            {
                return _List_Work_Orders;
            }
        }


        //public void LoadWorkOrders()
        //{
        //    lstWork_Order = new ObservableCollection<CAT_work_order>() { };

        //    lstWork_Order.Add(new CAT_work_order()
        //    {
        //        CAT_usr_gen_Id_Source = 25,
        //        CAT_usr_gen_Usr_Source = "XP12345",
        //        CAT_workorder_Type_Name = "Application",
        //        CAT_usr_gen_Usr_Target = "XP12345",
        //        tbl_usr_reg_FirstName = "Menganito",
        //        tbl_usr_reg_IpCountry = "MX",
        //        tbl_usr_reg_LastName = "Parras",
        //        tbl_usr_reg_SecondName = "Wegas",
        //        DateCreated = DateTime.Now,
        //        DateExpires = DateTime.Now.AddDays(20),
        //        Status = 0
        //    });
        //    lstWork_Order.Add(new CAT_work_order()
        //    {
        //        CAT_usr_gen_Id_Source = 34,
        //        CAT_usr_gen_Usr_Source = "JM12349812374",
        //        CAT_workorder_Type_Name = "Application",
        //        CAT_usr_gen_Usr_Target = "JM12349812374",
        //        tbl_usr_reg_FirstName = "Fulanito",
        //        tbl_usr_reg_IpCountry = "AF",
        //        tbl_usr_reg_LastName = "Cortes",
        //        tbl_usr_reg_SecondName = "Mega",
        //        DateCreated = DateTime.Now,
        //        DateExpires = DateTime.Now.AddDays(20),
        //        Status = 0
        //    });
        //}



    }
}
