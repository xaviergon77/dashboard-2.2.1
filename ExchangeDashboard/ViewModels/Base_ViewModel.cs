﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.Caching;
using System.Text;
using DAI.CORE.UIM.UIM_StaticInfo;
using Microsoft.Extensions.Configuration;

namespace ExchangeDashboard.ViewModels
{
    internal class Base_ViewModel : INotifyPropertyChanged
    {
        //private ObjectCache cache = MemoryCache.Default;
        //private IConfiguration _Configuration { get; set; }
        //private UIMCountry_detail[] _lstCountries { get; set; }
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        #endregion INotifyPropertyChanged

        public UIMCountry_detail[] lstCountries
        {
            //set
            //{
            //    _lstCountries = value;
            //    OnPropertyChanged("lstCountries");
            //}
            get
            {
                return DAI.CORE.UIM.Lib.Util.GetCountries();
            }
        }

        public Base_ViewModel()
        {
            //_Configuration = Lib.Util.GetConfiguration();

            //CacheItemPolicy policy = new CacheItemPolicy();
            //policy.AbsoluteExpiration = DateTimeOffset.Now.AddDays(7.0);
            //if (cache[ExchangeTools.Utils.COUNTRIES] != null)
            //{
            //    lstCountries = (UIMCountry_detail[])cache[ExchangeTools.Utils.COUNTRIES];
            //}
            //else
            //{
            //    lstCountries = DAI.CORE.Manager.Country.CountryLogicServices.getCountries(_Configuration, null);
            //    cache[ExchangeTools.Utils.COUNTRIES] = lstCountries;
            //}
        }
    }
}
