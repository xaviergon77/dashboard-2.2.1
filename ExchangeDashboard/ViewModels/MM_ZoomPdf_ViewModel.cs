﻿using ExchangeDashboard.Commands;
using ExchangeDB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ExchangeDashboard.ViewModels
{
    internal class MM_ZoomPdf_ViewModel : ExchangeDB.ViewModelBase
    {
        public MM_ZoomPdf_ViewModel()
        {
            MM_ZoomPdf_Some_Command = new ZoomPdf_Some_Command(this);
        }

        public ICommand MM_ZoomPdf_Some_Command
        {
            get;
            private set;
        }

        private string _Path;
        public string Path
        {
            set
            {
                _Path = value;
                OnPropertyChanged("Path");
            }
            get
            {
                return _Path;
            }
        }
    }
}
