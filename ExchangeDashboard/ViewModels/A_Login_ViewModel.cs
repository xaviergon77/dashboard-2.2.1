﻿using DAI.CORE.APIData;
using DAI.CORE.UIM.UIM_LogIn_Dash;
using ExchangeDashboard.Commands;
using ExchangeDashboard.Lib;
using ExchangeDB;
using ExchangeTools;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.Caching;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ExchangeDashboard.ViewModels
{
    internal class A_Login_ViewModel : ExchangeDB.ViewModelBase
    {
        public A_Login_ViewModel()
        {
            _Cmd_A_Login_GetQRToken = new GetQRToken_Command(this);
        }

        private IConfiguration _Configuration { get; set; }

        private ICommand _Cmd_A_Login_GetQRToken;

        private ICommand _Cmd_A_Login_Authenticate;

        public ICommand Cmd_A_Login_Authenticate
        {
            get
            {
                return _Cmd_A_Login_Authenticate ?? 
                    (_Cmd_A_Login_Authenticate = 
                    new Authenticate_Command(
                        () => MyAction(), 
                        () => CanExecute, 
                        this
                        )
                    );
            }
        }

        public bool CanExecute
        {
            get
            {
                if (string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password))
                {
                    return false;
                }
                return !(
                    ExchangeTools.Utils.checkForSQLInjection(UserName) 
                    || ExchangeTools.Utils.checkForSQLInjection(Password)
                    );
            }
        }

        public void MyAction()
        {
            _Configuration = Lib.Util.GetConfiguration();


            string userip = Tools.OutboundIp();
            UIMLogIn_Dash_request r = new UIMLogIn_Dash_request()
            {
                username = UserName.Trim(),
                password = ExchangeDB.Tools.MD5Hash(Password),
                sourceuri = Lib.Util.GetSourceUri(),
                userip = userip.Trim()
            };  

            UIMLogIn_Dash_response a = DAI.CORE.Manager.LogInDash.LoginDashLogicServices.Validate(_Configuration, r);

            if (a == null || a.data == null) return;
            bool Result = ((UIMLogIn_Dash_response)a).result;
            uint UsrId = Result ? ((UIMLogInDash_detail_response)a.data).cat_usr_gen_id : 0;
            bool IsGoogleAuth = ((UIMLogInDash_detail_response)a.data).isGoogleAuth;

            if (Result && UsrId > 0)
            {
                Lib.Util.ClearApplicationProperties();
                Application.Current.Properties.Add("UserIp", userip?.Trim());
                Application.Current.Properties.Add("UserId", UsrId);
                Application.Current.Properties.Add("UserName", UserName);
                Application.Current.Properties["AppName"] = "Exchange Dashboard - " + UserName;
                Application.Current.Properties.Add("IsTwoFactorAuth", false);
                Application.Current.Properties.Add("IsGoogleAuth", IsGoogleAuth);
                Application.Current.Properties.Add("Authenticated", false);

                if (IsGoogleAuth)
                {
                    _Cmd_A_Login_GetQRToken.Execute(this);
                }

                if (IsGoogleAuth && TokenValid)
                {
                    Application.Current.Properties["Authenticated"] = true;
                    Navigator.Navigate("Views/M_EndUsers.xaml");
                }
                else
                {
                    Application.Current.Properties["Authenticated"] = true;
                    Navigator.Navigate("Views/A_TwoFactorRegister.xaml");
                }
            }
        }

        private string _UserName;
        public string UserName
        {
            set
            {
                _UserName = value;
                OnPropertyChanged("UserName");
            }
            get
            {
                return _UserName;
            }
        }

        private string _Password;
        public string Password
        {
            set
            {
                _Password = value;
                OnPropertyChanged("Password");
            }
            get
            {
                return _Password;
            }
        }

        private string _Token;
        public string Token
        {
            set
            {
                _Token = value;
                OnPropertyChanged("Token");
            }
            get
            {
                return _Token;
            }
        }

        private bool _TokenValid;
        public bool TokenValid
        {
            set
            {
                _TokenValid = value;
                OnPropertyChanged("TokenValid");
            }
            get
            {
                return _TokenValid;
            }
        }
    }
}
