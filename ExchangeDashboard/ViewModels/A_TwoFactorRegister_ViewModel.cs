﻿using DAI.CORE.Manager.QRDash;
using DAI.CORE.UIM.UIM_QR_Dash;
using ExchangeDashboard.Commands;
using ExchangeDashboard.Lib;
using Microsoft.Extensions.Configuration;
using System.Drawing;
using System.Windows;
using System.Windows.Input;

namespace ExchangeDashboard.ViewModels
{
    internal class A_TwoFactorRegister_ViewModel : ExchangeDB.ViewModelBase
    {
        public A_TwoFactorRegister_ViewModel()
        {
            //MM_ZoomImage_Some_Command = new MM_ZoomImage_Some_Command(this);
            _Configuration = Lib.Util.GetConfiguration();
            Cmd_A_TwoFactor_GetQR.Execute(null);
        }

        private IConfiguration _Configuration { get; set; }

        private ICommand _Cmd_A_TwoFactor_GetQR;

        public ICommand Cmd_A_TwoFactor_GetQR
        {
            get
            {
                return _Cmd_A_TwoFactor_GetQR ??
                    (_Cmd_A_TwoFactor_GetQR =
                    new TwoFactor_GetQR_Command(
                        () => ActionGetQR(),
                        () => CanExecuteQR,
                        this
                        )
                    );
            }
        }

        public bool CanExecuteQR
        {
            get
            {
                return (bool)System.Windows.Application.Current.Properties["IsTwoFactorAuth"];
            }
        }

        public void ActionGetQR()
        {
            UIMQR_Dash_request r = new UIMQR_Dash_request() {
                sourceuri = Lib.Util.GetSourceUri(),
                userip = System.Windows.Application.Current.Properties["UserIp"].ToString(),
                username = System.Windows.Application.Current.Properties["UserId"].ToString()
            };
            UIMQR_Dash_response a = DAI.CORE.Manager.QRDash.QRDashLogicServices.GetQR(_Configuration, r);
            string Result = a.data;
            Image = ExchangeTools.ImageUtils.Base64StringToBitmap(a.data);
        }




        private ICommand _Cmd_A_TwoFactor_GetQRToken;

        public ICommand Cmd_A_TwoFactor_GetQRToken
        {
            get
            {
                return _Cmd_A_TwoFactor_GetQRToken ??
                    (_Cmd_A_TwoFactor_GetQRToken =
                    new TwoFactor_GetQRToken_Command(
                        () => ActionGetQRToken(),
                        () => CanExecuteQRToken,
                        this
                        )
                    );
            }
        }

        public bool CanExecuteQRToken
        {
            get
            {
                return true;
            }
        }

        public void ActionGetQRToken()
        {
            UIMQRToken_Dash_request r = new UIMQRToken_Dash_request()
            {
                sourceuri = Lib.Util.GetSourceUri(),
                userip = System.Windows.Application.Current.Properties["UserIp"].ToString(),
                username = System.Windows.Application.Current.Properties["UserId"].ToString(),
                token = Token
            };
            UIMQRToken_Dash_response a = QRTokenDashLogicServices.GetQRToken(_Configuration, r);
            string Result = a.data;
            TokenValid = a.data == "ok";
            if (TokenValid)
            {
                Navigator.Navigate("Views/M_EndUsers.xaml");
            }
        }



        public ICommand MM_ZoomImage_Some_Command
        {
            get;
            private set;
        }

        private Bitmap _Image;
        public Bitmap Image
        {
            set
            {
                _Image = value;
                OnPropertyChanged("Image");
            }
            get
            {
                return _Image;
            }
        }

        private string _Path;
        public string Path
        {
            set
            {
                _Path = value;
                OnPropertyChanged("Path");
            }
            get
            {
                return _Path;
            }
        }

        private string _Token;
        public string Token
        {
            set
            {
                _Token = value;
                OnPropertyChanged("Path");
            }
            get
            {
                return _Token;
            }
        }

        private bool _TokenValid;
        public bool TokenValid
        {
            set
            {
                _TokenValid = value;
                OnPropertyChanged("TokenValid");
            }
            get
            {
                return _TokenValid;
            }
        }

        public bool IsGoogleAuth
        {
            get
            {
                return Lib.Util.GetProperty("IsGoogleAuth") == "True";
            }
        }

        public Visibility GoogleAuthVisibility
        {
            get
            {
                if (IsGoogleAuth)
                {
                    return Visibility.Collapsed;
                }
                else
                {
                    return Visibility.Visible;
                }
            }
        }

    }
}
