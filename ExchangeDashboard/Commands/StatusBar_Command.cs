﻿using ExchangeDashboard.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace ExchangeDashboard.Commands
{
    internal class StatusBar_Command : ICommand
    {
        public StatusBar_Command(StatusBar_ViewModel viewModel)
        {
            _ViewModel = viewModel;
        }

        private StatusBar_ViewModel _ViewModel;

        #region ICommand Members
        public event System.EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        public bool CanExecute(object parameter)
        {
            //return _ViewModel.CanHistorialViewFiles;
            return true;
        }
        public void Execute(object paramter)
        {
            //_ViewModel.ProduceFile();
        }
        #endregion
    }
}
