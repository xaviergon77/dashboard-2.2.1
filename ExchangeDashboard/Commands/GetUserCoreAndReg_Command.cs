﻿using DAI.CORE.Manager.CoreRegEu;
using DAI.CORE.Manager.WorkOrder;
using DAI.CORE.UIM.UIM_CoreRegEu;
using DAI.CORE.UIM.UIM_WorkOrder;
using ExchangeDashboard.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace ExchangeDashboard.Commands
{
    internal class GetUserCoreAndReg_Command : CommandBase
    {
        private M_WorkOrders_ViewModel _ViewModel;
        private IConfiguration _Configuration { get; set; }

        public GetUserCoreAndReg_Command(M_WorkOrders_ViewModel viewModel)
        {
            _action = GetNewWorkOrders_Action;
            _canExecute = GetNewWorkOrders_CanExecute;
            _ViewModel = viewModel;
        }

        internal bool GetNewWorkOrders_CanExecute()
        {
            return true;
        }

        internal void GetNewWorkOrders_Action()
        {
            if (_ViewModel == null) return;
            if (_ViewModel.SelectedWorkOrder == null) return;
            _Configuration = Lib.Util.GetConfiguration();

            UIMCoreRegEu_request r = new UIMCoreRegEu_request()
            {
                CAT_usr_gen_Id = _ViewModel.SelectedWorkOrder.caT_usr_gen_Id.ToString(),
                sourceuri = Lib.Util.GetSourceUri(),
                AdminUser = Lib.Util.GetProperty("UserName")
            };

            UIMCoreRegEu_response a = CoreRegEuLogicServices.LoadData(_Configuration, r);

            if (a.result == true)
            {
                //_ViewModel.SelectedCoreAndReg = a.data;
                _ViewModel.SelectedRegAndCore = a.data;
            }
        }
    }
}
