﻿using DAI.CORE.Manager.WorkOrder;
using DAI.CORE.UIM.UIM_WorkOrder;
using ExchangeDashboard.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace ExchangeDashboard.Commands
{
    internal class GetWorkOrders : CommandBase
    {
        private M_WorkOrders_ViewModel _ViewModel;
        private IConfiguration _Configuration { get; set; }

        public GetWorkOrders(M_WorkOrders_ViewModel viewModel)
        {
            //if (viewModel == null) return;
            //if (viewModel.SelectedWorkOrder == null) return;
            if (viewModel != null)
            {
                viewModel.SelectedDocuments = null;
                viewModel.SelectedChecks = null;
                viewModel.SelectedRegAndCore = null;
                viewModel.SelectedWorkOrder = null;
            }

            _action = GetNewWorkOrders_Action;
            _canExecute = GetNewWorkOrders_CanExecute;
            _ViewModel = viewModel;
        }

        internal bool GetNewWorkOrders_CanExecute()
        {
            return true;
        }

        /*
         * This is the old one (copied from the WorkOrder ViewModel)
         */
        internal void GetNewWorkOrders_Action()
        {
            _Configuration = Lib.Util.GetConfiguration();

            UIMWorkOrder_request r = new UIMWorkOrder_request()
            {
                sourceuri = Lib.Util.GetSourceUri(),
            };

            UIMWorkOrder_response a = DAI.CORE.Manager.WorkOrder.WorkOrderLogicServices.LoadData(_Configuration, r);

            if (((UIMWorkOrder_response)a).result == true)
            {
                _ViewModel.lstWork_Order = new ObservableCollection<UIMWorkOrder_detail_response_simple>(a.data);
            }
        }
    }
}
