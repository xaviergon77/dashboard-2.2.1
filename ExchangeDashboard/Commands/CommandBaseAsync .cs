﻿using AsyncAwaitBestPractices;
using AsyncAwaitBestPractices.MVVM;
using ExchangeDashboard.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ExchangeDashboard.Commands
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync();
        bool CanExecute();
    }

    internal class CommandBaseAsync : IAsyncCommand
    {
        public event EventHandler CanExecuteChanged;

        protected bool _isExecuting;
        protected Func<Task> _action;
        protected Func<bool> _canExecute;
        protected IErrorHandler _errorHandler;

        //public CommandBaseAsync(
        //    Func<Task> execute,
        //    Func<bool> canExecute = null,
        //    IErrorHandler errorHandler = null)
        //{
        //    _action = execute;
        //    _canExecute = canExecute;
        //    _errorHandler = errorHandler;
        //}

        public bool CanExecute()
        {
            return !_isExecuting && (_canExecute?.Invoke() ?? true);
        }

        public async Task ExecuteAsync()
        {
            if (CanExecute())
            {
                try
                {
                    _isExecuting = true;
                    await _action();
                }
                finally
                {
                    _isExecuting = false;
                }
            }

            RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        #region Explicit implementations
        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }

        void ICommand.Execute(object parameter)
        {
            ExecuteAsync().FireAndForgetSafeAsync(_errorHandler);
        }
        #endregion


    }

    public interface IErrorHandler
    {
        void HandleError(Exception ex);
    }

    public static class TaskUtilities
    {
#pragma warning disable RECS0165 // Asynchronous methods should return a Task instead of void
        public static async void FireAndForgetSafeAsync(this Task task, IErrorHandler handler = null)
#pragma warning restore RECS0165 // Asynchronous methods should return a Task instead of void
        {
            try
            {
                await task;
            }
            catch (Exception ex)
            {
                handler?.HandleError(ex);
            }
        }
    }


}