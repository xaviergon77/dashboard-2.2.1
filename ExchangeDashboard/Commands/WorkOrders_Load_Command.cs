﻿using ExchangeDashboard.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace ExchangeDashboard.Commands
{
    internal class WorkOrders_Load_Command : CommandBase
    {

        private M_WorkOrders_ViewModel _ViewModel;

        public WorkOrders_Load_Command(Action action, Func<bool> canExecute, M_WorkOrders_ViewModel viewModel)
        {
            _action = action;
            _canExecute = canExecute;
            _ViewModel = viewModel;
        }
    }
}
