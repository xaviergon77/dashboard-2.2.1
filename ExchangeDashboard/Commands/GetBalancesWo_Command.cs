﻿using DAI.CORE.UIM.UIM_BalanceDash;
using ExchangeDashboard.Lib;
using ExchangeDashboard.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace ExchangeDashboard.Commands
{
    internal class GetBalancesWo_Command : CommandBase
    {
        private M_WorkOrders_ViewModel _ViewModel;
        private IConfiguration _Configuration { get; set; }

        public GetBalancesWo_Command(M_WorkOrders_ViewModel viewModel)
        {
            _action = Balances_Action;
            _canExecute = Balances_CanExecute;
            _ViewModel = viewModel;
        }

        internal bool Balances_CanExecute()
        {
            return true;
        }

        internal void Balances_Action()
        {
            if (_ViewModel == null) return;
            if (_ViewModel.SelectedWorkOrder == null) return;
            //if (_ViewModel.SelectedRegAndCore.isOnBoarded == 1) return;
            _Configuration = Lib.Util.GetConfiguration();

            UIMBalanceDash_request r = new UIMBalanceDash_request()
            {
                sourceuri = Lib.Util.GetSourceUri(),
                userip = System.Windows.Application.Current.Properties["UserIp"].ToString(),
                ukey = _ViewModel.SelectedWorkOrder.caT_usr_gen_Id.ToString()
            };
            UIMBalanceDash_response a = DAI.CORE.Manager.BalanceDash.BalanceDashLogicServices.LoadData(_Configuration, r);
            //UIMBalanceDash_detail_response[] Result = a.data;
            _ViewModel.Balances = a.data;
            //_ViewModel.SelectedFilter = "Specific Approved User";
        }
    }
}
