﻿using DAI.CORE.Manager.CoreRegEu;
using DAI.CORE.Manager.PropertyVal;
using DAI.CORE.Manager.WorkOrder;
using DAI.CORE.UIM.UIM_CoreRegEu;
using DAI.CORE.UIM.UIM_PropertyVal;
using DAI.CORE.UIM.UIM_WorkOrder;
using ExchangeDashboard.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace ExchangeDashboard.Commands
{
    //Implement this from main ".cs" file on MouseOut action
    internal class UpdateUserCoreAndReg_Command : CommandBase
    {
        private M_WorkOrders_ViewModel _ViewModel;
        private IConfiguration _Configuration { get; set; }

        private string _Field;
        private string _User_Id;
        private string _Value;

        public UpdateUserCoreAndReg_Command(M_WorkOrders_ViewModel viewModel, string f, string u, string v)
        {
            _action = UpdateWorkOrderProperty_Action;
            _canExecute = UpdateWorkOrderProperty_CanExecute;
            _ViewModel = viewModel;
            _Field = f;
            _User_Id = u;
            _Value = v;
        }

        internal bool UpdateWorkOrderProperty_CanExecute()
        {
            return true;
        }

        internal void UpdateWorkOrderProperty_Action()
        {
            if (_ViewModel == null) return;
            if (_ViewModel.SelectedWorkOrder == null) return;
            _Configuration = Lib.Util.GetConfiguration();

            UIMCoreRegUpdate_request r = new UIMCoreRegUpdate_request()
            {
                CAT_usr_gen_Id = _ViewModel.SelectedWorkOrder.caT_usr_gen_Id.ToString(),
                field = this._Field,
                Value = this._Value,
                sourceuri = Lib.Util.GetSourceUri(),
                AdminUser = Lib.Util.GetProperty("UserName")
            };

            UIMCoreRegUpdate_response a = CoreRegUpdateLogicServices.UpdateData(_Configuration, r);

            if (a.result == true && a.data.value == this._Value)
            {
                //do nothing
            }
            else
            {
                throw new NotImplementedException("Implement UpdateUserCoreAndReg_Command error when updating");
            }
        }
    }
}
