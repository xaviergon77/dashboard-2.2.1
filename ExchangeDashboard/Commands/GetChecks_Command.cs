﻿using DAI.CORE.Manager.CoreRegEu;
using DAI.CORE.Manager.PropertyVal;
using DAI.CORE.Manager.WorkOrder;
using DAI.CORE.UIM.UIM_CoreRegEu;
using DAI.CORE.UIM.UIM_PropertyVal;
using DAI.CORE.UIM.UIM_WorkOrder;
using ExchangeDashboard.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace ExchangeDashboard.Commands
{
    internal class GetChecks_Command : CommandBase
    {
        private M_WorkOrders_ViewModel _ViewModel;
        private IConfiguration _Configuration { get; set; }

        public GetChecks_Command(M_WorkOrders_ViewModel viewModel)
        {
            _action = GetChecks_Action;
            _canExecute = GetChecks_CanExecute;
            _ViewModel = viewModel;
        }

        internal bool GetChecks_CanExecute()
        {
            return true;
        }

        internal void GetChecks_Action()
        {
            if (_ViewModel == null) return;
            if (_ViewModel.SelectedWorkOrder == null) return;
            _Configuration = Lib.Util.GetConfiguration();

            UIMPropertyValWo_request r = new UIMPropertyValWo_request()
            {
                CAT_usr_gen_Id = _ViewModel.SelectedWorkOrder.caT_usr_gen_Id.ToString(),
                sourceuri = Lib.Util.GetSourceUri(),
                AdminUser = Lib.Util.GetProperty("UserName")
            };

            UIMPropertyValWo_response a = PropertyValWoLogicServices.LoadData(_Configuration, r);

            if (a.result == true)
            {
                //_ViewModel.lstChecks = new ObservableCollection<UIMPropertyValWo_detail_response>(a.data);
                _ViewModel.SelectedChecks = new ObservableCollection<UIMPropertyValWo_detail_response>(a.data);
            }
        }
    }
}
