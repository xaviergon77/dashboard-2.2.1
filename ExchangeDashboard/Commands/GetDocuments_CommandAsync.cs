﻿using DAI.CORE.Manager.CoreRegEu;
using DAI.CORE.Manager.ImagesEu;
using DAI.CORE.Manager.PropertyVal;
using DAI.CORE.Manager.WorkOrder;
using DAI.CORE.UIM.ImagesEu;
using DAI.CORE.UIM.UIM_CoreRegEu;
using DAI.CORE.UIM.UIM_ImagesEu;
using DAI.CORE.UIM.UIM_PropertyVal;
using DAI.CORE.UIM.UIM_WorkOrder;
using ExchangeDashboard.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExchangeDashboard.Commands
{
    internal class GetDocuments_CommandAsync : CommandBaseAsync
    {
        private M_WorkOrders_ViewModel _ViewModel;
        private IConfiguration _Configuration { get; set; }

        public GetDocuments_CommandAsync(M_WorkOrders_ViewModel viewModel)
        {
            _action = GetDocuments_Action;
            _canExecute = GetDocuments_CanExecute;
            _errorHandler = null;

            _action = GetDocuments_Action;
            _canExecute = GetDocuments_CanExecute;
            _ViewModel = viewModel;
        }
            
        internal bool GetDocuments_CanExecute()
        {
            return true;
        }

        internal async Task GetDocuments_Action()
        {
            if (_ViewModel == null) return;
            if (_ViewModel.SelectedWorkOrder == null) return;
            _Configuration = Lib.Util.GetConfiguration();

            UIMImagesEu_request r = new UIMImagesEu_request()
            {
                CAT_usr_gen_Id = _ViewModel.SelectedWorkOrder.caT_usr_gen_Id.ToString(),
                sourceuri = Lib.Util.GetSourceUri(),
                AdminUser = Lib.Util.GetProperty("UserName")
            };

            UIMImagesEu_response a = ImagesEuLogicServices.LoadData(_Configuration, r);

            if (a.result == true)
            {
                //_ViewModel.arrDocuments = a.data;
                _ViewModel.SelectedDocuments = new ObservableCollection<UIMImagesEu_detail_response>(a.data);
            }
        }
    }
}
