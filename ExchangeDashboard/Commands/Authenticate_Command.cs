﻿using ExchangeDashboard.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace ExchangeDashboard.Commands
{
    internal class Authenticate_Command : CommandBase
    {
        private A_Login_ViewModel _ViewModel;

        public Authenticate_Command(Action action, Func<bool> canExecute, A_Login_ViewModel viewModel)
        {
            _action = action;
            _canExecute = canExecute;
            _ViewModel = viewModel;
        }
    }
}