﻿using DAI.CORE.Manager.WorkOrder;
using DAI.CORE.UIM.UIM_WorkOrder;
using ExchangeDashboard.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace ExchangeDashboard.Commands
{
    internal class GetWorkOrdersNew_Command : CommandBase
    {
        private M_WorkOrders_ViewModel _ViewModel;
        private IConfiguration _Configuration { get; set; }

        public GetWorkOrdersNew_Command(M_WorkOrders_ViewModel viewModel)
        {
            _action = GetNewWorkOrders_Action;
            _canExecute = GetNewWorkOrders_CanExecute;
            _ViewModel = viewModel;
        }

        internal bool GetNewWorkOrders_CanExecute()
        {
            return true;
        }

        internal void GetNewWorkOrders_Action()
        {
            if (_ViewModel == null) return;
            if (_ViewModel.SelectedWorkOrder == null) return;
            _Configuration = Lib.Util.GetConfiguration();

            UIMWorkOrderNew_request r = new UIMWorkOrderNew_request()
            {
                sourceuri = Lib.Util.GetSourceUri(),
                AdminUser = Lib.Util.GetProperty("UserName")
            };

            UIMWorkOrderNew_response a = DAI.CORE.Manager.WorkOrder.WorkOrderNewLogicServices.LoadData(_Configuration, r);

            if (((UIMWorkOrderNew_response)a).result == true)
            {
                _ViewModel.List_Work_Orders = new ObservableCollection<UIMWorkOrder_detail_response_simple>(a.data);
            }
        }
    }
}
