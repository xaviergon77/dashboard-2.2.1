﻿using DAI.CORE.UIM.UIM_EndUserApprove_Dash;
using ExchangeDashboard.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace ExchangeDashboard.Commands
{
    internal class Approve_Application_Command : CommandBase
    {
        private M_EndUsers_ViewModel _ViewModel;
        private IConfiguration _Configuration { get; set; }

        public Approve_Application_Command(M_EndUsers_ViewModel viewModel)
        {
            _action = EndUser_Approve_Action;
            _canExecute = EndUser_Approve_CanExecute;
            _ViewModel = viewModel;
        }

        internal bool EndUser_Approve_CanExecute()
        {
            return true;
        }

        internal void EndUser_Approve_Action()
        {
            if (_ViewModel == null) return;
            if (_ViewModel.My_M_WorkOrders_ViewModel == null) return;
            if (_ViewModel.My_M_WorkOrders_ViewModel.SelectedRegAndCore.isOnBoarded == 0) return;

            _Configuration = Lib.Util.GetConfiguration();

            UIMEndUserApprove_Dash_request r = new UIMEndUserApprove_Dash_request()
            {
                EndUser = _ViewModel.My_M_WorkOrders_ViewModel.SelectedWorkOrder.caT_usr_gen_Id.ToString(),
                AdminUser = Application.Current.Properties["UserId"].ToString(),
                sourceuri = Lib.Util.GetSourceUri()
            };

            UIMEndUserApprove_Dash_response a = DAI.CORE.Manager.EndUserApproveDash.EndUserApproveDashLogicServices.Approve(_Configuration, r);

            bool Result = ((UIMEndUserApprove_Dash_response)a).result;
            bool IsApproved = Result ? a.data == "ok" : false;

            if (Result && IsApproved)

            {
                //_ViewModel.My_M_WorkOrders_ViewModel.SelectedRegAndCore.isOnBoarded = 1;
                //_ViewModel.My_M_WorkOrders_ViewModel.SelectedRegAndCore.Bind_isActive = true;
                //_ViewModel.CMD_M_EndUser_GetBalance.Execute(_ViewModel);
                _ViewModel.My_M_WorkOrders_ViewModel.SelectedRegAndCore.isOnBoarded = 1;
                new GetBalancesWo_Command(_ViewModel.My_M_WorkOrders_ViewModel).Execute(null);
                _ViewModel.SelectedFilter = "Specific Approved User";
            }
            else
            {
                _ViewModel.My_M_WorkOrders_ViewModel.SelectedRegAndCore.isOnBoarded = 0;
                _ViewModel.ApprovedChecked = false;
            }
        }
    }
}
