﻿using ExchangeDashboard.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace ExchangeDashboard.Commands
{
    internal class TwoFactor_GetQRToken_Command : CommandBase
    {
        private A_TwoFactorRegister_ViewModel _ViewModel;

        public TwoFactor_GetQRToken_Command(Action action, Func<bool> canExecute, A_TwoFactorRegister_ViewModel viewModel)
        {
            _action = action;
            _canExecute = canExecute;
            _ViewModel = viewModel;
        }
    }
}