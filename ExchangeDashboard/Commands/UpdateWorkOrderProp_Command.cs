﻿using DAI.CORE.Manager.PropertyVal;
using DAI.CORE.Manager.WorkOrder;
using DAI.CORE.UIM.UIM_PropertyVal;
using DAI.CORE.UIM.UIM_WorkOrder;
using ExchangeDashboard.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace ExchangeDashboard.Commands
{
    internal class UpdateWorkOrderProp_Command : CommandBase
    {
        private UIMPropertyValWo_detail_response _ViewModel;
        private IConfiguration _Configuration { get; set; }

        //private int _Id;
        //private string _User_Id;
        //private string _Value;

        public UpdateWorkOrderProp_Command(UIMPropertyValWo_detail_response viewModel)
        {
            _action = UpdateWorkOrderProperty_Action;
            _canExecute = UpdateWorkOrderProperty_CanExecute;
            _ViewModel = viewModel;
            //_Id = i;
            //_User_Id = u;
            //_Value = v;
        }

        internal bool UpdateWorkOrderProperty_CanExecute()
        {
            return true;
        }

        internal void UpdateWorkOrderProperty_Action()
        {
            _Configuration = Lib.Util.GetConfiguration();

            UIMPropValUpdate_request r = new UIMPropValUpdate_request()
            {
                Id = _ViewModel.id.ToString(),
                CAT_usr_gen_Id = _ViewModel.caT_usr_gen_Id.ToString(),
                Value = _ViewModel.value,
                sourceuri = Lib.Util.GetSourceUri(),
                AdminUser = Lib.Util.GetProperty("UserName")
            };

            UIMPropValUpdate_response a = PropValUpdateLogicServices.UpdateData(_Configuration, r);

            if (a.result == true && a.data.id == this._ViewModel.id)
            {
                //do nothing
            }
            else
            {
                throw new NotImplementedException("Implement UpdateWorkOrderProp_Command error when updating");
            }
        }
    }
}
