﻿using ExchangeDashboard.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace ExchangeDashboard.Commands
{
    internal class WorkOrders_View_Command : CommandBase
    {

        private M_WorkOrders_ViewModel _ViewModel;

        public WorkOrders_View_Command(Action action, Func<bool> canExecute, M_WorkOrders_ViewModel viewModel)
        {
            _action = action;
            _canExecute = canExecute;
            _ViewModel = viewModel;
        }
    }
}
