﻿using ExchangeDashboard.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace ExchangeDashboard.Commands
{
    internal class CommandBase : ICommand
    {
        protected Action _action;
        protected Func<bool> _canExecute;

        #region ICommand Members
        public event System.EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        public bool CanExecute(object parameter)
        {
            return _canExecute.Invoke();
        }
        public void Execute(object paramter)
        {
            if (_action == null) return;
            _action();
        }
        #endregion
    }
}
