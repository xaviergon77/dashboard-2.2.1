﻿using DAI.CORE.Manager.QRDash;
using DAI.CORE.UIM.UIM_QR_Dash;
using ExchangeDashboard.Lib;
using ExchangeDashboard.ViewModels;
using Microsoft.Extensions.Configuration;

namespace ExchangeDashboard.Commands
{
    internal class GetQRToken_Command : CommandBase
    {
        private A_Login_ViewModel _ViewModel;
        private IConfiguration _Configuration { get; set; }

        public GetQRToken_Command(A_Login_ViewModel viewModel)
        {
            _action = Login_GetQR_Action;
            _canExecute = Login_GetQR_CanExecute;
            _ViewModel = viewModel;
        }

        internal bool Login_GetQR_CanExecute()
        {
            return true;
        }

        internal void Login_GetQR_Action()
        {
            if (_ViewModel == null) return;
            if (string.IsNullOrEmpty(_ViewModel.Token?.Trim())) return;

            _Configuration = Lib.Util.GetConfiguration();

            UIMQRToken_Dash_request r = new UIMQRToken_Dash_request()
            {
                sourceuri = Lib.Util.GetSourceUri(),
                userip = System.Windows.Application.Current.Properties["UserIp"].ToString(),
                username = System.Windows.Application.Current.Properties["UserId"].ToString(),
                token = _ViewModel.Token
            };
            UIMQRToken_Dash_response a = QRTokenDashLogicServices.GetQRToken(_Configuration, r);
            string Result = a.data;
            _ViewModel.TokenValid = a.data == "ok";
            if (!_ViewModel.TokenValid)
            {
                Navigator.Navigate("Views/A_TwoFactorRegister.xaml");
            }

        }
    }
}
