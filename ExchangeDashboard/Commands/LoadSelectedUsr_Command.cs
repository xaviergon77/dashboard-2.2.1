﻿using ExchangeDashboard.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace ExchangeDashboard.Commands
{
    internal class LoadSelectedUsr_Command : CommandBase
    {
        private M_EndUsers_ViewModel _ViewModel;

        public LoadSelectedUsr_Command(Action action, Func<bool> canExecute, M_EndUsers_ViewModel viewModel)
        {
            _action = action;
            _canExecute = canExecute;
            _ViewModel = viewModel;
        }
    }
}
