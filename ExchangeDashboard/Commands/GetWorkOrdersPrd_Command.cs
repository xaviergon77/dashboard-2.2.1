﻿using DAI.CORE.Manager.WorkOrder;
using DAI.CORE.UIM.UIM_WorkOrder;
using ExchangeDashboard.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace ExchangeDashboard.Commands
{
    internal class GetWorkOrdersPrd_Command : CommandBase
    {
        private M_WorkOrders_ViewModel _ViewModel;
        private IConfiguration _Configuration { get; set; }

        public GetWorkOrdersPrd_Command(M_WorkOrders_ViewModel viewModel)
        {
            if (viewModel != null)
            {
                viewModel.SelectedDocuments = null;
                viewModel.SelectedChecks = null;
                viewModel.SelectedRegAndCore = null;
                viewModel.SelectedWorkOrder = null;
            }

            _action = GetWorkOrdersPrd_Action;
            _canExecute = GetWorkOrdersPrd_CanExecute;
            _ViewModel = viewModel;
        }

        internal bool GetWorkOrdersPrd_CanExecute()
        {
            return true;
        }

        internal void GetWorkOrdersPrd_Action()
        {
            _Configuration = Lib.Util.GetConfiguration();

            UIMWorkOrderPrd_request r = new UIMWorkOrderPrd_request()
            {
                /* TODO: debe tomarse el caT_usr_gen_Id de algo como "SelectedUser"*/
                sourceuri = Lib.Util.GetSourceUri(),
                AdminUser = Lib.Util.GetProperty("UserName")
            };

            UIMWorkOrderPrd_response a = DAI.CORE.Manager.WorkOrder.WorkOrderPrdLogicServices.LoadData(_Configuration, r);

            if (a.result == true)
            {
                //_ViewModel.List_Work_Orders = new ObservableCollection<UIMWorkOrder_detail_response_simple>(a.data);
                _ViewModel.lstWork_Order = new ObservableCollection<UIMWorkOrder_detail_response_simple>(a.data);
            }
        }
    }
}
