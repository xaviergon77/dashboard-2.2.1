﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ExchangeDB
{
    public partial class tbl_aaa_country : ViewModelBase
    {
        public string FIFA
        {
            set
            {
                _FIFA = value;
                OnPropertyChanged("FIFA");
            }
            get
            {
                return _FIFA;
            }
        }
        public string Dial
        {
            set
            {
                _Dial = value;
                OnPropertyChanged("Dial");
            }
            get
            {
                return _Dial;
            }
        }
        public string Loc3
        {
            set
            {
                _Dial = value;
                OnPropertyChanged("Dial");
            }
            get
            {
                return _Dial;
            }
        }
        public string Marc
        {
            set
            {
                _Marc = value;
                OnPropertyChanged("Marc");
            }
            get
            {
                return _Marc;
            }
        }
        public string IsIndependent
        {
            set
            {
                _IsIndependent = value;
                OnPropertyChanged("IsIndependent");
            }
            get
            {
                return _IsIndependent;
            }
        }
        public string LocNum
        {
            set
            {
                _LocNum = value;
                OnPropertyChanged("LocNum");
            }
            get
            {
                return _LocNum;
            }
        }
        public string Gaul
        {
            set
            {
                _Gaul = value;
                OnPropertyChanged("Gaul");
            }
            get
            {
                return _Gaul;
            }
        }
        public string Fips
        {
            set
            {
                _Fips = value;
                OnPropertyChanged("Fips");
            }
            get
            {
                return _Fips;
            }
        }
        public string Wmo
        {
            set
            {
                _Wmo = value;
                OnPropertyChanged("Wmo");
            }
            get
            {
                return _Wmo;
            }
        }
        public string Loc2
        {
            set
            {
                _Loc2 = value;
                OnPropertyChanged("Loc2");
            }
            get
            {
                return _Loc2;
            }
        }
        public string Itu
        {
            set
            {
                _Itu = value;
                OnPropertyChanged("Itu");
            }
            get
            {
                return _Itu;
            }
        }
        public string Ioc
        {
            set
            {
                _Ioc = value;
                OnPropertyChanged("Ioc");
            }
            get
            {
                return _Ioc;
            }
        }
        public string Ds
        {
            set
            {
                _Ds = value;
                OnPropertyChanged("Ds");
            }
            get
            {
                return _Ds;
            }
        }
        public string UnitermSpanish
        {
            set
            {
                _UnitermSpanish = value;
                OnPropertyChanged("UnitermSpanish");
            }
            get
            {
                return _UnitermSpanish;
            }
        }
        public string GlobalCode
        {
            set
            {
                _GlobalCode = value;
                OnPropertyChanged("GlobalCode");
            }
            get
            {
                return _GlobalCode;
            }
        }
        public string InterRegionCode
        {
            set
            {
                _InterRegionCode = value;
                OnPropertyChanged("InterRegionCode");
            }
            get
            {
                return _InterRegionCode;
            }
        }
        public string OfficialFr
        {
            set
            {
                _OfficialFr = value;
                OnPropertyChanged("OfficialFr");
            }
            get
            {
                return _OfficialFr;
            }
        }
        public string UnitermFrSh
        {
            set
            {
                _UnitermFrSh = value;
                OnPropertyChanged("UnitermFrSh");
            }
            get
            {
                return _UnitermFrSh;
            }
        }
        public string LocCurr
        {
            set
            {
                _LocCurr = value;
                OnPropertyChanged("LocCurr");
            }
            get
            {
                return _LocCurr;
            }
        }
        public string Developing
        {
            set
            {
                _Developing = value;
                OnPropertyChanged("Developing");
            }
            get
            {
                return _Developing;
            }
        }
        public string UnitermRu
        {
            set
            {
                _UnitermRu = value;
                OnPropertyChanged("UnitermRu");
            }
            get
            {
                return _UnitermRu;
            }
        }
        public string UnitermEnSh
        {
            set
            {
                _UnitermEnSh = value;
                OnPropertyChanged("UnitermEnSh");
            }
            get
            {
                return _UnitermEnSh;
            }
        }
        public string LocCurrAlpha
        {
            set
            {
                _LocCurrAlpha = value;
                OnPropertyChanged("LocCurrAlpha");
            }
            get
            {
                return _LocCurrAlpha;
            }
        }
        public string SmallIsland
        {
            set
            {
                _SmallIsland = value;
                OnPropertyChanged("SmallIsland");
            }
            get
            {
                return _SmallIsland;
            }
        }
        public string UnitermSp
        {
            set
            {
                _UnitermSp = value;
                OnPropertyChanged("UnitermSp");
            }
            get
            {
                return _UnitermSp;
            }
        }
        public string IsoCurNum
        {
            set
            {
                _LocCurNum = value;
                OnPropertyChanged("LocCurNum");
            }
            get
            {
                return _LocCurNum;
            }
        }
        public string UnitermCh
        {
            set
            {
                _UnitermCh = value;
                OnPropertyChanged("UnitermCh");
            }
            get
            {
                return _UnitermCh;
            }
        }
        public string UnitermFr
        {
            set
            {
                _UnitermFr = value;
                OnPropertyChanged("UnitermFr");
            }
            get
            {
                return _UnitermFr;
            }
        }
        public string UnitermRuSh
        {
            set
            {
                _UnitermRuSh = value;
                OnPropertyChanged("UnitermRuSh");
            }
            get
            {
                return _UnitermRuSh;
            }
        }
        public string M49
        {
            set
            {
                _M49 = value;
                OnPropertyChanged("M49");
            }
            get
            {
                return _M49;
            }
        }
        public string SubRegionCode
        {
            set
            {
                _SubRegionCode = value;
                OnPropertyChanged("SubRegionCode");
            }
            get
            {
                return _SubRegionCode;
            }
        }
        public string RegionCode
        {
            set
            {
                _RegionCode = value;
                OnPropertyChanged("RegionCode");
            }
            get
            {
                return _RegionCode;
            }
        }
        public string OfficialAr
        {
            set
            {
                _OfficialAr = value;
                OnPropertyChanged("OfficialAr");
            }
            get
            {
                return _OfficialAr;
            }
        }
        public string LocCurrMinor
        {
            set
            {
                _LocCurrMinor = value;
                OnPropertyChanged("LocCurrMinor");
            }
            get
            {
                return _LocCurrMinor;
            }
        }
        public string UnitermAr
        {
            set
            {
                _UnitermAr = value;
                OnPropertyChanged("UnitermAr");
            }
            get
            {
                return _UnitermAr;
            }
        }
        public string UnitermChSh
        {
            set
            {
                _UnitermChSh = value;
                OnPropertyChanged("UnitermChSh");
            }
            get
            {
                return _UnitermChSh;
            }
        }
        public string LandLockedDeveloping
        {
            set
            {
                _LandLockedDeveloping = value;
                OnPropertyChanged("LandLockedDeveloping");
            }
            get
            {
                return _LandLockedDeveloping;
            }
        }
        public string IntermRegion
        {
            set
            {
                _IntermRegion = value;
                OnPropertyChanged("IntermRegion");
            }
            get
            {
                return _IntermRegion;
            }
        }
        public string OfficialEs
        {
            set
            {
                _OfficialEs = value;
                OnPropertyChanged("OfficialEs");
            }
            get
            {
                return _OfficialEs;
            }
        }
        public string UnitermEn
        {
            set
            {
                _UnitermEn = value;
                OnPropertyChanged("UnitermEn");
            }
            get
            {
                return _UnitermEn;
            }
        }
        public string OfficialCh
        {
            set
            {
                _OfficialCh = value;
                OnPropertyChanged("OfficialCh");
            }
            get
            {
                return _OfficialCh;
            }
        }
        public string OfficialEn
        {
            set
            {
                _OfficialEn = value;
                OnPropertyChanged("OfficialEn");
            }
            get
            {
                return _OfficialEn;
            }
        }
        public string LocCurrCountry
        {
            set
            {
                _LocCurrCountry = value;
                OnPropertyChanged("LocCurrCountry");
            }
            get
            {
                return _LocCurrCountry;
            }
        }
        public string LeastDeveloped
        {
            set
            {
                _LeastDeveloped = value;
                OnPropertyChanged("LeastDeveloped");
            }
            get
            {
                return _LeastDeveloped;
            }
        }
        public string RegionName
        {
            set
            {
                _RegionName = value;
                OnPropertyChanged("RegionName");
            }
            get
            {
                return _RegionName;
            }
        }
        public string UnitermArSh
        {
            set
            {
                _UnitermArSh = value;
                OnPropertyChanged("UnitermArSh");
            }
            get
            {
                return _UnitermArSh;
            }
        }
        public string SubRegion
        {
            set
            {
                _SubRegion = value;
                OnPropertyChanged("SubRegion");
            }
            get
            {
                return _SubRegion;
            }
        }
        public string OfficialRu
        {
            set
            {
                _OfficialRu = value;
                OnPropertyChanged("OfficialRu");
            }
            get
            {
                return _OfficialRu;
            }
        }
        public string GlobalName
        {
            set
            {
                _GlobalName = value;
                OnPropertyChanged("GlobalName");
            }
            get
            {
                return _GlobalName;
            }
        }
        public string Capital
        {
            set
            {
                _Capital = value;
                OnPropertyChanged("Capital");
            }
            get
            {
                return _Capital;
            }
        }
        public string Continent
        {
            set
            {
                _Continent = value;
                OnPropertyChanged("Continent");
            }
            get
            {
                return _Continent;
            }
        }
        public string Tld
        {
            set
            {
                _Tld = value;
                OnPropertyChanged("Tld");
            }
            get
            {
                return _Tld;
            }
        }
        public string Lang
        {
            set
            {
                _Lang = value;
                OnPropertyChanged("Lang");
            }
            get
            {
                return _Lang;
            }
        }
        public int GeonameId
        {
            set
            {
                _GeonameId = value;
                OnPropertyChanged("GeonameId");
            }
            get
            {
                return _GeonameId;
            }
        }
        public string CldrDisplay
        {
            set
            {
                _CldrDisplay = value;
                OnPropertyChanged("CldrDisplay");
            }
            get
            {
                return _CldrDisplay;
            }
        }
        public string Edgar
        {
            set
            {
                _Edgar = value;
                OnPropertyChanged("Edgar");
            }
            get
            {
                return _Edgar;
            }
        }

        //#region INotifyPropertyChangedMembers
        //public event PropertyChangedEventHandler PropertyChanged;
        //protected void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;

        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }

        //    //switch (propertyName)
        //    //{
        //    //}
        //}
        //#endregion
    }

}
