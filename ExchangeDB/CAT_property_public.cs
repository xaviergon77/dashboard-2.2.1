﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ExchangeDB
{
    public partial class CAT_property : ViewModelBase
    {
        public decimal Id
        {
            set
            {
                _Id = value;
                OnPropertyChanged("Id");
            }
            get
            {
                return _Id;
            }
        }
        public int CAT_schema_object_Id
        {
            set
            {
                _CAT_schema_object_Id = value;
                OnPropertyChanged("CAT_schema_object_Id");
            }
            get
            {
                return _CAT_schema_object_Id;
            }
        }
        public int CAT_step_Id
        {
            set
            {
                _CAT_step_Id = value;
                OnPropertyChanged("CAT_step_Id");
            }
            get
            {
                return _CAT_step_Id;
            }
        }
        public string CAT_step_Name
        {
            set
            {
                _CAT_step_Name = value;
                OnPropertyChanged("CAT_step_Name");
            }
            get
            {
                return _CAT_step_Name;
            }
        }
        public int CAT_action_Id
        {
            set
            {
                _CAT_action_Id = value;
                OnPropertyChanged("CAT_action_Id");
            }
            get
            {
                return _CAT_action_Id;
            }
        }
        public string CAT_action_Name
        {
            set
            {
                _CAT_action_Name = value;
                OnPropertyChanged("CAT_action_Name");
            }
            get
            {
                return _CAT_action_Name;
            }
        }
        public bool IsTenant
        {
            set
            {
                _IsTenant = value;
                OnPropertyChanged("IsTenant");
            }
            get
            {
                return _IsTenant;
            }
        }
        public int CAT_tenant_Id
        {
            set
            {
                _CAT_tenant_Id = value;
                OnPropertyChanged("CAT_tenant_Id");
            }
            get
            {
                return _CAT_tenant_Id;
            }
        }
        public bool IsVerifiable
        {
            set
            {
                _IsVerifiable = value;
                OnPropertyChanged("IsVerifiable");
            }
            get
            {
                return _IsVerifiable;
            }
        }
        public string Type
        {
            set
            {
                _Type = value;
                OnPropertyChanged("Type");
            }
            get
            {
                return _Type;
            }
        }
        public string Name
        {
            set
            {
                _Name = value;
                OnPropertyChanged("Name");
            }
            get
            {
                return _Name;
            }
        }
        public string Description
        {
            set
            {
                _Description = value;
                OnPropertyChanged("Description");
            }
            get
            {
                return _Description;
            }
        }
        public string RegExp
        {
            set
            {
                _RegExp = value;
                OnPropertyChanged("RegExp");
            }
            get
            {
                return _RegExp;
            }
        }
        public string Behavior
        {
            set
            {
                _Behavior = value;
                OnPropertyChanged("Behavior");
            }
            get
            {
                return _Behavior;
            }
        }
        public string DataSource
        {
            set
            {
                _DataSource = value;
                OnPropertyChanged("DataSource");
            }
            get
            {
                return _DataSource;
            }
        }

        //#region INotifyPropertyChangedMembers
        //public event PropertyChangedEventHandler PropertyChanged;
        //protected void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;

        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }

        //    //switch (propertyName)
        //    //{
        //    //}
        //}
        //#endregion
    }

}
