﻿using System;

namespace ExchangeDB
{

    public partial class tbl_property_val
    {
        private decimal _Id { get; set; }
        private int _CAT_schema_object_Id { get; set; }
        private decimal _CAT_property_Id { get; set; }
        private decimal _RefHeader_Id { get; set; }
        private decimal _RefDetail_Id { get; set; }
        private int _CAT_step_Id { get; set; }
        private string _StepName { get; set; }
        private int _CAT_action_Id { get; set; }
        private string _ActionName { get; set; }
        private string _Name { get; set; }
        private ulong _CAT_usr_gen_Id { get; set; }
        private string _Type { get; set; }
        private string _Value { get; set; }
        private bool _IsVerifiable { get; set; }
        private string _Behavior { get; set; }
        private string _Message { get; set; }
        private string _Notes { get; set; }
        private string _SourceUri { get; set; }
        private string _RegExp { get; set; }
        private ulong _CAT_usr_gen_Id_Created { get; set; }
        private DateTime _DateCreated { get; set; }
    }

}
