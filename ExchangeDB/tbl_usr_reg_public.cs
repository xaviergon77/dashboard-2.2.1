﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExchangeDB
{
    public partial class tbl_usr_reg : ViewModelBase
    {
        public DateTime DateBirth
        {
            set
            {
                _DateBirth = value;
                OnPropertyChanged("DateBirth");
            }
            get
            {
                return _DateBirth;
            }
        }
        public ulong CAT_usr_gen_Id
        {
            set
            {
                _CAT_usr_gen_Id = value;
                OnPropertyChanged("CAT_usr_gen_Id");
            }
            get
            {
                return _CAT_usr_gen_Id;
            }
        }
        public ulong Id
        {
            set
            {
                _Id = value;
                OnPropertyChanged("Id");
            }
            get
            {
                return _Id;
            }
        }
        public Int16 FovIdx
        {
            set
            {
                _FovIdx = value;
                OnPropertyChanged("FovIdx");
            }
            get
            {
                return _FovIdx;
            }
        }
        public Int16 FovIdxSub
        {
            set
            {
                _FovIdxSub = value;
                OnPropertyChanged("FovIdxSub");
            }
            get
            {
                return _FovIdxSub;
            }
        }
        public DateTime DateCreated
        {
            set
            {
                _DateCreated = value;
                OnPropertyChanged("DateCreated");
            }
            get
            {
                return _DateCreated;
            }
        }
        public string Address
        {
            set
            {
                _Address = value;
                OnPropertyChanged("Address");
            }
            get
            {
                return _Address;
            }
        }
        public string Address2
        {
            set
            {
                _Address2 = value;
                OnPropertyChanged("Address2");
            }
            get
            {
                return _Address2;
            }
        }
        public string Agr
        {
            set
            {
                _Agr = value;
                OnPropertyChanged("Agr");
            }
            get
            {
                return _Agr;
            }
        }
        public string City
        {
            set
            {
                _City = value;
                OnPropertyChanged("City");
            }
            get
            {
                return _City;
            }
        }
        public string Email
        {
            set
            {
                _Email = value;
                OnPropertyChanged("Email");
            }
            get
            {
                return _Email;
            }
        }
        public string EmailBackup
        {
            set
            {
                _EmailBackup = value;
                OnPropertyChanged("EmailBackup");
            }
            get
            {
                return _EmailBackup;
            }
        }
        public string FirstName
        {
            set
            {
                _FirstName = value;
                OnPropertyChanged("FirstName");
            }
            get
            {
                return _FirstName;
            }
        }
        public string Fov
        {
            set
            {
                _Fov = value;
                OnPropertyChanged("Fov");
            }
            get
            {
                return _Fov;
            }
        }
        public string FovType
        {
            set
            {
                _FovType = value;
                OnPropertyChanged("FovType");
            }
            get
            {
                return _FovType;
            }
        }
        public string HomePhone
        {
            set
            {
                _HomePhone = value;
                OnPropertyChanged("HomePhone");
            }
            get
            {
                return _HomePhone;
            }
        }
        public string HomePhone_First5
        {
            get
            {
                string[] parts = _HomePhone.Split(' ', 2);
                if (parts.Length > 1)
                {
                    if (parts[1].Length > 5)
                    {
                        return parts[1].Substring(0, 5);
                    }
                }
                return "";
            }
        }
        public string LocCitizen
        {
            set
            {
                _LocCitizen = value;
                OnPropertyChanged("IsoCitizen");
            }
            get
            {
                return _LocCitizen;
            }
        }
        public string LocIssue
        {
            set
            {
                _LocIssue = value;
                OnPropertyChanged("IsoIssue");
            }
            get
            {
                return _LocIssue;
            }
        }
        public string LocResident
        {
            set
            {
                _LocResident = value;
                OnPropertyChanged("LocResident");
            }
            get
            {
                return _LocResident;
            }
        }
        public string LastName
        {
            set
            {
                _LastName = value;
                OnPropertyChanged("LastName");
            }
            get
            {
                return _LastName;
            }
        }
        public string MaritalStatus
        {
            set
            {
                _MaritalStatus = value;
                OnPropertyChanged("MaritalStatus");
            }
            get
            {
                return _MaritalStatus;
            }
        }
        public string MobilePhone
        {
            set
            {
                _MobilePhone = value;
                OnPropertyChanged("MobilePhone");
            }
            get
            {
                return _MobilePhone;
            }
        }
        public string Passport
        {
            set
            {
                _Passport = value;
                OnPropertyChanged("Passport");
            }
            get
            {
                return _Passport;
            }
        }
        public string PostalCode
        {
            set
            {
                _PostalCode = value;
                OnPropertyChanged("PostalCode");
            }
            get
            {
                return _PostalCode;
            }
        }
        public string Profession
        {
            set
            {
                _Profession = value;
                OnPropertyChanged("Profession");
            }
            get
            {
                return _Profession;
            }
        }
        public string SecondId
        {
            set
            {
                _SecondId = value;
                OnPropertyChanged("SecondId");
            }
            get
            {
                return _SecondId;
            }
        }
        public string SecondIdType
        {
            set
            {
                _SecondIdType = value;
                OnPropertyChanged("SecondIdType");
            }
            get
            {
                return _SecondIdType;
            }
        }
        public string SecondLocIssue
        {
            set
            {
                _SecondLocIssue = value;
                OnPropertyChanged("SecondLocIssue");
            }
            get
            {
                return _SecondLocIssue;
            }
        }
        public string SecondLastName
        {
            set
            {
                _SecondLastName = value;
                OnPropertyChanged("SecondLastName");
            }
            get
            {
                return _SecondLastName;
            }
        }
        public string SecondName
        {
            set
            {
                _SecondName = value;
                OnPropertyChanged("SecondName");
            }
            get
            {
                return _SecondName;
            }
        }
        public string SourceIncome
        {
            set
            {
                _SourceIncome = value;
                OnPropertyChanged("SourceIncome");
            }
            get
            {
                return _SourceIncome;
            }
        }
        public string State
        {
            set
            {
                _State = value;
                OnPropertyChanged("State");
            }
            get
            {
                return _State;
            }
        }
        public string WorkPhone
        {
            set
            {
                _WorkPhone = value;
                OnPropertyChanged("WorkPhone");
            }
            get
            {
                return _WorkPhone;
            }
        }
    }

}
