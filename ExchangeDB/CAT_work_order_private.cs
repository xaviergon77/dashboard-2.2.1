﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeDB
{
    public partial class CAT_work_order
    {
        private byte _Status { get; set; }
        private ulong _CAT_usr_gen_Id_Created { get; set; }
        private ulong _CAT_usr_gen_Id_Source { get; set; }
        private ulong _CAT_usr_gen_Id_Target { get; set; }
        private ulong _CAT_workorder_Type_Id { get; set; }
        private decimal _Id { get; set; }
        private DateTime _DateCreated { get; set; }
        private DateTime _DateExpires { get; set; }
        private string _tbl_usr_reg_FirstName { get; set; }
        private string _tbl_usr_reg_SecondName { get; set; }
        private string _tbl_usr_reg_LastName { get; set; }
        private string _tbl_usr_reg_IpCountry { get; set; }
        private string _CAT_usr_gen_Usr_Source { get; set; }
        private string _CAT_usr_gen_Usr_Target { get; set; }
        private string _CAT_workorder_Type_Name { get; set; }
        private string _Description { get; set; }
        private string _UsrImpersonate { get; set; }
    }

}
