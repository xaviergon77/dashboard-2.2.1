﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeDB
{
    public partial class tbl_aaa_country
    {
        private string _FIFA { get; set; }
        private string _Dial { get; set; }
        private string _Loc3 { get; set; }
        private string _Marc { get; set; }
        private string _IsIndependent { get; set; }
        private string _LocNum { get; set; }
        private string _Gaul { get; set; }
        private string _Fips { get; set; }
        private string _Wmo { get; set; }
        private string _Loc2 { get; set; }
        private string _Itu { get; set; }
        private string _Ioc { get; set; }
        private string _Ds { get; set; }
        private string _UnitermSpanish { get; set; }
        private string _GlobalCode { get; set; }
        private string _InterRegionCode { get; set; }
        private string _OfficialFr { get; set; }
        private string _UnitermFrSh { get; set; }
        private string _LocCurr { get; set; }
        private string _Developing { get; set; }
        private string _UnitermRu { get; set; }
        private string _UnitermEnSh { get; set; }
        private string _LocCurrAlpha { get; set; }
        private string _SmallIsland { get; set; }
        private string _UnitermSp { get; set; }
        private string _LocCurNum { get; set; }
        private string _UnitermCh { get; set; }
        private string _UnitermFr { get; set; }
        private string _UnitermRuSh { get; set; }
        private string _M49 { get; set; }
        private string _SubRegionCode { get; set; }
        private string _RegionCode { get; set; }
        private string _OfficialAr { get; set; }
        private string _LocCurrMinor { get; set; }
        private string _UnitermAr { get; set; }
        private string _UnitermChSh { get; set; }
        private string _LandLockedDeveloping { get; set; }
        private string _IntermRegion { get; set; }
        private string _OfficialEs { get; set; }
        private string _UnitermEn { get; set; }
        private string _OfficialCh { get; set; }
        private string _OfficialEn { get; set; }
        private string _LocCurrCountry { get; set; }
        private string _LeastDeveloped { get; set; }
        private string _RegionName { get; set; }
        private string _UnitermArSh { get; set; }
        private string _SubRegion { get; set; }
        private string _OfficialRu { get; set; }
        private string _GlobalName { get; set; }
        private string _Capital { get; set; }
        private string _Continent { get; set; }
        private string _Tld { get; set; }
        private string _Lang { get; set; }
        private int _GeonameId { get; set; }
        private string _CldrDisplay { get; set; }
        private string _Edgar { get; set; }
    }

}
