﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeDB
{
    public partial class tbl_usr_reg
    {
        private DateTime _DateBirth { get; set; }
        private ulong _CAT_usr_gen_Id { get; set; }
        private ulong _Id { get; set; }
        private Int16 _FovIdx { get; set; }
        private Int16 _FovIdxSub { get; set; }
        private DateTime _DateCreated { get; set; }
        private string _Address { get; set; }
        private string _Address2 { get; set; }
        private string _Agr { get; set; }
        private string _City { get; set; }
        private string _Email { get; set; }
        private string _EmailBackup { get; set; }
        private string _FirstName { get; set; }
        private string _Fov { get; set; }
        private string _FovType { get; set; }
        private string _HomePhone { get; set; }
        private string _LocCitizen { get; set; }
        private string _LocIssue { get; set; }
        private string _LocResident { get; set; }
        private string _LastName { get; set; }
        private string _MaritalStatus { get; set; }
        private string _MobilePhone { get; set; }
        private string _Passport { get; set; }
        private string _PostalCode { get; set; }
        private string _Profession { get; set; }
        private string _SecondId { get; set; }
        private string _SecondIdType { get; set; }
        private string _SecondLocIssue { get; set; }
        private string _SecondLastName { get; set; }
        private string _SecondName { get; set; }
        private string _SourceIncome { get; set; }
        private string _State { get; set; }
        private string _WorkPhone { get; set; }
    }

}
