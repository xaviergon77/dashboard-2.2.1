﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace ExchangeDB
{
    #region ViewModelBase Class
    public class ViewModelBase : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        #endregion INotifyPropertyChanged
    }
    #endregion ViewModelBase Class

    //#region GridItemViewModel Class
    //public class GridItemViewModel : ViewModelBase
    //{
    //    #region Title Property
    //    private String _title = null;
    //    public String Title
    //    {
    //        get { return _title; }
    //        set
    //        {
    //            if (value != _title)
    //            {
    //                _title = value;
    //                OnPropertyChanged();
    //            }
    //        }
    //    }
    //    #endregion Description Property

    //    #region Description Property
    //    private String _description = null;
    //    public String Description
    //    {
    //        get { return _description; }
    //        set
    //        {
    //            if (value != _description)
    //            {
    //                _description = value;
    //                OnPropertyChanged();
    //            }
    //        }
    //    }
    //    #endregion Description Property

    //    #region Path Property
    //    private String _path = null;
    //    public String Path
    //    {
    //        get { return _path; }
    //        set
    //        {
    //            if (value != _path)
    //            {
    //                _path = value;
    //                OnPropertyChanged();
    //            }
    //        }
    //    }
    //    #endregion Path Property

    //    #region ImageSource Property
    //    private Bitmap _imageSource = null;
    //    public Bitmap ImageSource
    //    {
    //        get { return _imageSource; }
    //        set
    //        {
    //            if (value != _imageSource)
    //            {
    //                _imageSource = value;
    //                OnPropertyChanged();
    //            }
    //        }
    //    }
    //    #endregion ImageSource Property
    //}
    //#endregion GridItemViewModel Class






    //#region TestItemViewModel Class
    //public class TestItemViewModel : ViewModelBase
    //{
    //    #region Title Property
    //    private String _title = null;
    //    public String Title
    //    {
    //        get { return _title; }
    //        set
    //        {
    //            if (value != _title)
    //            {
    //                _title = value;
    //                OnPropertyChanged();
    //            }
    //        }
    //    }
    //    #endregion Description Property

    //    #region Description Property
    //    private String _description = null;
    //    public String Description
    //    {
    //        get { return _description; }
    //        set
    //        {
    //            if (value != _description)
    //            {
    //                _description = value;
    //                OnPropertyChanged();
    //            }
    //        }
    //    }
    //    #endregion Description Property

    //    #region Path Property
    //    private String _path = null;
    //    public String Path
    //    {
    //        get { return _path; }
    //        set
    //        {
    //            if (value != _path)
    //            {
    //                _path = value;
    //                OnPropertyChanged();
    //            }
    //        }
    //    }
    //    #endregion Path Property

    //    #region ImageSource Property
    //    private Bitmap _imageSource = null;
    //    public Bitmap ImageSource
    //    {
    //        get { return _imageSource; }
    //        set
    //        {
    //            if (value != _imageSource)
    //            {
    //                _imageSource = value;
    //                OnPropertyChanged();
    //            }
    //        }
    //    }
    //    #endregion ImageSource Property
    //}
    //#endregion TestItemViewModel Class
}
