﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Text;

namespace ExchangeDB
{
    public partial class tbl_eusr_tmp
    {
        private ulong _CAT_ausr_Id_Created { get; set; }
        private ulong _CAT_usr_gen_Id { get; set; }
        private ulong _Id { get; set; }
        private bool _IsActive { get; set; }
        private bool _IsOnBoarded { get; set; }
        private DateTime _DateCreated { get; set; }
        private string _Email { get; set; }
        private string _GUID { get; set; }
        private string _SourceUri { get; set; }
        private string _Usr { get; set; }
        private string _Name { get; set; }
        private string _Ip { get; set; }
        private string _Loc2 { get; set; }
        private tbl_usr_reg _Registration { get; set; }

        private string temporary_account {get; set;}
        private Bitmap _Signature { get; set; }
        private ObservableCollection<GridItemViewModel> _Docs { get; set; }
    }
}
