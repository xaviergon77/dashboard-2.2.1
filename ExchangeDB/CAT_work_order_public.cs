﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ExchangeDB
{
    public partial class CAT_work_order : ViewModelBase
    {
        public byte Status
        {
            set
            {
                _Status = value;
                OnPropertyChanged("Status");
            }
            get
            {
                return _Status;
            }
        }
        public ulong CAT_usr_gen_Id_Created
        {
            set
            {
                _CAT_usr_gen_Id_Created = value;
                OnPropertyChanged("CAT_usr_gen_Id_Created");
            }
            get
            {
                return _CAT_usr_gen_Id_Created;
            }
        }
        public ulong CAT_usr_gen_Id_Source
        {
            set
            {
                _CAT_usr_gen_Id_Source = value;
                OnPropertyChanged("CAT_usr_gen_Id_Source");
            }
            get
            {
                return _CAT_usr_gen_Id_Source;
            }
        }
        public ulong CAT_usr_gen_Id_Target
        {
            set
            {
                _CAT_usr_gen_Id_Target = value;
                OnPropertyChanged("CAT_usr_gen_Id_Target");
            }
            get
            {
                return _CAT_usr_gen_Id_Target;
            }
        }
        public ulong CAT_workorder_Type_Id
        {
            set
            {
                _CAT_workorder_Type_Id = value;
                OnPropertyChanged("CAT_workorder_Type_Id");
            }
            get
            {
                return _CAT_workorder_Type_Id;
            }
        }
        public decimal Id
        {
            set
            {
                _Id = value;
                OnPropertyChanged("Id");
            }
            get
            {
                return _Id;
            }
        }
        public DateTime DateCreated
        {
            set
            {
                _DateCreated = value;
                OnPropertyChanged("DateCreated");
            }
            get
            {
                return _DateCreated;
            }
        }
        public DateTime DateExpires
        {
            set
            {
                _DateExpires = value;
                OnPropertyChanged("DateExpires");
            }
            get
            {
                return _DateExpires;
            }
        }
        public string tbl_usr_reg_FirstName
        {
            set
            {
                _tbl_usr_reg_FirstName = value;
                OnPropertyChanged("tbl_usr_reg_FirstName");
            }
            get
            {
                return _tbl_usr_reg_FirstName;
            }
        }
        public string tbl_usr_reg_SecondName
        {
            set
            {
                _tbl_usr_reg_SecondName = value;
                OnPropertyChanged("tbl_usr_reg_SecondName");
            }
            get
            {
                return _tbl_usr_reg_SecondName;
            }
        }
        public string tbl_usr_reg_LastName
        {
            set
            {
                _tbl_usr_reg_LastName = value;
                OnPropertyChanged("tbl_usr_reg_LastName");
            }
            get
            {
                return _tbl_usr_reg_LastName;
            }
        }
        public string tbl_usr_reg_IpCountry
        {
            set
            {
                _tbl_usr_reg_IpCountry = value;
                OnPropertyChanged("tbl_usr_reg_IpCountry");
            }
            get
            {
                return _tbl_usr_reg_IpCountry;
            }
        }
        public string CAT_usr_gen_Usr_Source
        {
            set
            {
                _CAT_usr_gen_Usr_Source = value;
                OnPropertyChanged("CAT_usr_gen_Usr_Source");
            }
            get
            {
                return _CAT_usr_gen_Usr_Source;
            }
        }
        public string CAT_usr_gen_Usr_Target
        {
            set
            {
                _CAT_usr_gen_Usr_Target = value;
                OnPropertyChanged("CAT_usr_gen_Usr_Target");
            }
            get
            {
                return _CAT_usr_gen_Usr_Target;
            }
        }
        public string CAT_workorder_Type_Name
        {
            set
            {
                _CAT_workorder_Type_Name = value;
                OnPropertyChanged("CAT_workorder_Type_Name");
            }
            get
            {
                return _CAT_workorder_Type_Name;
            }
        }
        public string Description
        {
            set
            {
                _Description = value;
                OnPropertyChanged("Description");
            }
            get
            {
                return _Description;
            }
        }
        public string UsrImpersonate
        {
            set
            {
                _UsrImpersonate = value;
                OnPropertyChanged("UsrImpersonate");
            }
            get
            {
                return _UsrImpersonate;
            }
        }

        //#region INotifyPropertyChangedMembers
        //public event PropertyChangedEventHandler PropertyChanged;
        //protected void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;

        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }

        //    //switch (propertyName)
        //    //{
        //    //}
        //}
        //#endregion
    }
}
