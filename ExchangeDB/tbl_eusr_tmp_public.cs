﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;

namespace ExchangeDB
{
    public partial class tbl_eusr_tmp : ViewModelBase
    {
        public ulong CAT_ausr_Id_Created
        {
            set
            {
                _CAT_ausr_Id_Created = value;
                OnPropertyChanged("CAT_ausr_Id_Created");
            }
            get
            {
                return _CAT_ausr_Id_Created;
            }
        }
        public ulong CAT_usr_gen_Id
        {
            set
            {
                _CAT_usr_gen_Id = value;
                OnPropertyChanged("CAT_usr_gen_Id");
            }
            get
            {
                return _CAT_usr_gen_Id;
            }
        }
        public ulong Id
        {
            set
            {
                _Id = value;
                OnPropertyChanged("Id");
            }
            get
            {
                return _Id;
            }
        }
        public bool IsActive
        {
            set
            {
                _IsActive = value;
                OnPropertyChanged("IsActive");
            }
            get
            {
                return _IsActive;
            }
        }
        public bool IsOnBoarded
        {
            set
            {
                _IsOnBoarded = value;
                OnPropertyChanged("IsOnBoarded");
            }
            get
            {
                return _IsOnBoarded;
            }
        }
        public DateTime DateCreated
        {
            set
            {
                _DateCreated = value;
                OnPropertyChanged("DateCreated");
            }
            get
            {
                return _DateCreated;
            }
        }
        public string Email
        {
            set
            {
                _Email = value;
                OnPropertyChanged("Email");
            }
            get
            {
                return _Email;
            }
        }
        public string GUID
        {
            set
            {
                _GUID = value;
                OnPropertyChanged("GUID");
            }
            get
            {
                return _GUID;
            }
        }
        public string SourceUri
        {
            set
            {
                _SourceUri = value;
                OnPropertyChanged("SourceUri");
            }
            get
            {
                return _SourceUri;
            }
        }
        public string Usr
        {
            set
            {
                _Usr = value;
                OnPropertyChanged("Usr");
            }
            get
            {
                return _Usr;
            }
        }

        public string Name
        {
            set
            {
                _Name = value;
                OnPropertyChanged("Name");
            }
            get
            {
                return _Name;
            }
        }
        public string Ip
        {
            set
            {
                _Ip = value;
                OnPropertyChanged("Ip");
            }
            get
            {
                return _Ip;
            }
        }
        public string Loc2
        {
            set
            {
                _Loc2 = value;
                OnPropertyChanged("Loc2");
            }
            get
            {
                return _Loc2;
            }
        }
        public tbl_usr_reg Registration
        {
            set
            {
                _Registration = value;
                OnPropertyChanged("Registration");
            }
            get
            {
                return _Registration;
            }
        }

        public Bitmap Signature
        {
            set
            {
                _Signature = value;
                OnPropertyChanged("Signature");
            }
            get
            {
                return _Signature;
            }
        }

        public ObservableCollection<GridItemViewModel> Docs
        {
            set
            {
                _Docs = value;
                OnPropertyChanged("Docs");
            }
            get
            {
                return _Docs;
            }
        }

        public string BK
        {
            get
            {
                if (Registration != null)
                {
                    return string.Format("{0}{1}{2}", Loc2, Registration.HomePhone_First5, Tools.ToBase36(CAT_usr_gen_Id));
                }
                return "";
            }
        }
        public string ACCT
        {
            get
            {
                if (Registration != null)
                {
                    return string.Format("{0}{1}{2}{3}{4}", "MFIB", Loc2, Registration.HomePhone_First5, Tools.ToBase36(CAT_usr_gen_Id), Tools.ToBase36(251));
                }
                return "";
            }
        }

        public string Uid
        {
            get
            {
                if (Registration != null)
                {
                    return string.Format("{0}", Tools.ToBase36(CAT_usr_gen_Id));
                }
                return "";
            }
        }

        //#region INotifyPropertyChangedMembers
        //public event PropertyChangedEventHandler PropertyChanged;
        //protected void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;

        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }

        //    //switch (propertyName)
        //    //{
        //    //}
        //}
        //#endregion

    }
}
