﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeDB
{
    public partial class CAT_property
    {
        private decimal _Id { get; set; }
        private int _CAT_schema_object_Id { get; set; }
        private int _CAT_step_Id { get; set; }
        private int _CAT_action_Id { get; set; }
        private bool _IsTenant { get; set; }
        private int _CAT_tenant_Id { get; set; }
        private string _CAT_step_Name { get; set; }
        private string _CAT_action_Name { get; set; }
        private string _Type { get; set; }
        private string _Name { get; set; }
        private string _Description { get; set; }
        private bool _IsVerifiable { get; set; }
        private string _RegExp { get; set; }
        private string _Behavior { get; set; }
        private string _DataSource { get; set; }
    }

}
