﻿using System;
using System.ComponentModel;

namespace ExchangeDB
{

    public partial class tbl_property_val : ViewModelBase
    {
        public decimal Id
        {
            set
            {
                _Id = value;
                OnPropertyChanged("Id");
            }
            get
            {
                return _Id;
            }
        }
        public int CAT_schema_object_Id
        {
            set
            {
                _CAT_schema_object_Id = value;
                OnPropertyChanged("CAT_schema_object_Id");
            }
            get
            {
                return _CAT_schema_object_Id;
            }
        }
        public decimal CAT_property_Id
        {
            set
            {
                _CAT_property_Id = value;
                OnPropertyChanged("CAT_property_Id");
            }
            get
            {
                return _CAT_property_Id;
            }
        }
        public decimal RefHeader_Id
        {
            set
            {
                _RefHeader_Id = value;
                OnPropertyChanged("RefHeader_Id");
            }
            get
            {
                return _RefHeader_Id;
            }
        }
        public decimal RefDetail_Id
        {
            set
            {
                _RefDetail_Id = value;
                OnPropertyChanged("RefDetail_Id");
            }
            get
            {
                return _RefDetail_Id;
            }
        }
        public int CAT_step_Id
        {
            set
            {
                _CAT_step_Id = value;
                OnPropertyChanged("CAT_step_Id");
            }
            get
            {
                return _CAT_step_Id;
            }
        }
        public string StepName
        {
            set
            {
                _StepName = value;
                OnPropertyChanged("StepName");
            }
            get
            {
                return _StepName;
            }
        }
        public int CAT_action_Id
        {
            set
            {
                _CAT_action_Id = value;
                OnPropertyChanged("CAT_action_Id");
            }
            get
            {
                return _CAT_action_Id;
            }
        }
        public string ActionName
        {
            set
            {
                _ActionName = value;
                OnPropertyChanged("ActionName");
            }
            get
            {
                return _ActionName;
            }
        }
        public string Name
        {
            set
            {
                _Name = value;
                OnPropertyChanged("Name");
            }
            get
            {
                return _Name;
            }
        }
        public ulong CAT_usr_gen_Id
        {
            set
            {
                _CAT_usr_gen_Id = value;
                OnPropertyChanged("CAT_usr_gen_Id");
            }
            get
            {
                return _CAT_usr_gen_Id;
            }
        }
        public string Type
        {
            set
            {
                _Type = value;
                OnPropertyChanged("Type");
            }
            get
            {
                return _Type;
            }
        }
        public string Value
        {
            set
            {
                _Type = value;
                OnPropertyChanged("Type");
            }
            get
            {
                return _Type;
            }
        }
        public bool IsVerifiable
        {
            set
            {
                _IsVerifiable = value;
                OnPropertyChanged("IsVerifiable");
            }
            get
            {
                return _IsVerifiable;
            }
        }
        public string Behavior
        {
            set
            {
                _Behavior = value;
                OnPropertyChanged("Behavior");
            }
            get
            {
                return _Behavior;
            }
        }
        public string Message
        {
            set
            {
                _Message = value;
                OnPropertyChanged("Message");
            }
            get
            {
                return _Message;
            }
        }
        public string Notes
        {
            set
            {
                _Notes = value;
                OnPropertyChanged("Notes");
            }
            get
            {
                return _Notes;
            }
        }
        public string SourceUri
        {
            set
            {
                _SourceUri = value;
                OnPropertyChanged("SourceUri");
            }
            get
            {
                return _SourceUri;
            }
        }
        public string RegExp
        {
            set
            {
                _RegExp = value;
                OnPropertyChanged("RegExp");
            }
            get
            {
                return _RegExp;
            }
        }
        public ulong CAT_usr_gen_Id_Created
        {
            set
            {
                _CAT_usr_gen_Id_Created = value;
                OnPropertyChanged("CAT_usr_gen_Id_Created");
            }
            get
            {
                return _CAT_usr_gen_Id_Created;
            }
        }
        public DateTime DateCreated
        {
            set
            {
                _DateCreated = value;
                OnPropertyChanged("DateCreated");
            }
            get
            {
                return _DateCreated;
            }
        }

    }

}
